package binh.com.music.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Update

import binh.com.music.data.Track

/**
 * Created by binh on 2/26/2019.
 */

@Dao
interface BaseDao<T> {

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(model: T): Int

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(list: List<T>): Int

    @Insert
    fun insert(model: T): Long

    @Insert
    fun insert(list: List<T>): LongArray

    @Delete
    fun delete(model: T): Int

    @Delete
    fun delete(list: List<T>): Int

}
