package binh.com.music.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.Query
import android.support.v4.media.MediaMetadataCompat
import binh.com.music.data.db.BaseDao
import java.io.Serializable

/**
 * Created by binh on 2/21/2019.
 */

@Entity(tableName = "track")
class Track(@PrimaryKey var id: Long = 0,
            var title: String = "",
            var text: String = "",
            var file: String? = null,
            var image: String? = null,
            var duration: Long = 0,
            var artist: String,
            var artistId: Long = 0,
            var album: String,
            var albumId: Long = 0,
            var ringtone: Int = 0,
            var path: String
) : Serializable {

    var count = 0
    var isFavorite = false
    var isLastPlay = false
    var position = 0

    val isRingtone: Boolean
        get() = ringtone != 0

    val metadataCompat: MediaMetadataCompat
        get() = MediaMetadataCompat.Builder()
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, this.duration)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, this.album)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, this.image)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, this.artist)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, this.title)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI, this.path)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, this.id.toString())
                .build()

    @Dao
    interface TrackDao : BaseDao<Track> {
        @Query("SELECT * FROM track")
        fun loadAll(): List<Track>

        @Query("SELECT * FROM track WHERE id = :id")
        fun getById(id: Long): Track

        @Query("UPDATE track SET isFavorite = :favorite WHERE id = :mediaId")
        fun setFavorite(mediaId: Long, favorite: Boolean): Int

        @Query("SELECT * FROM track WHERE isFavorite = :favorite")
        fun loadFavorite(favorite: Boolean): List<Track>

        @Query("DELETE FROM track")
        fun deleteAll()

        @Query("delete from track where id = :id")
        fun delete(id: Long): Int


    }

    @Entity(tableName = "current_track")
    class CurrentTrack : Serializable {
        var track: Track?

        constructor(track: Track) {
            this.track = track
        }

        @PrimaryKey(autoGenerate = true)
        var id: Long = 0
    }

    @Dao
    interface CurrentTrackDao : BaseDao<CurrentTrack> {
        @Query("Select track from current_track")
        fun tracks(): List<Track>

        @Query("delete from current_track")
        fun deleteAll(): Int

        @Query("delete from current_track where id = :mediaId")
        fun delete(mediaId: Long): Int
    }
}
