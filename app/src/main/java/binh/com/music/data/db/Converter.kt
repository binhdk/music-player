package binh.com.music.data.db

import android.arch.persistence.room.TypeConverter
import binh.com.music.data.Track
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by binh on 2/25/2019.
 * Convert complex type to simple type in entity
 */

object Converter {
    @JvmStatic
    @TypeConverter
    fun jsonToTracks(value: String): List<Track>? {
        val listType = object : TypeToken<List<Track>>() {

        }.type
        return Gson().fromJson<List<Track>>(value, listType)
    }

    @JvmStatic
    @TypeConverter
    fun tracksToJson(list: List<Track>): String {
        return Gson().toJson(list)
    }

    @JvmStatic
    @TypeConverter
    fun jsonToTrack(value: String): Track? {
        val listType = object : TypeToken<Track>() {

        }.type
        return Gson().fromJson<Track>(value, listType)
    }

    @JvmStatic
    @TypeConverter
    fun trackToJson(track: Track): String {
        return Gson().toJson(track)
    }


}