package binh.com.music.data

import java.io.Serializable

/**
 * Created by binh on 2/21/2019.
 */

class Artist(var id: Long = 0,
             var name: String = "Unknown",
             var numSongs: Int = 0,
             var numAlbums: Int = 0,
             var image: String? = null) : Serializable
