package binh.com.music.data

import java.io.Serializable

/**
 * Created by binh on 2/21/2019.
 */

class Album(var id: Long = 0,
            var name: String = "Unknown",
            var artist: String = "Unknown",
            var numSong: Int = 0,
            var tracks: MutableList<Track>? = ArrayList(),
            var art: String? = null) : Serializable {
}
