package binh.com.music.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.Query
import binh.com.music.data.db.BaseDao
import java.io.Serializable
import java.util.*

/**
 * Created by binh on 2/21/2019.
 */

@Entity(tableName = "playlist")
class PlayList() : Serializable {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    var name: String = ""
    var tracks: MutableList<Track> = ArrayList()
        set(value) {
            tracks.clear()
            tracks.addAll(value)
        }
    var isFavorite = false

    val numTracks: Int
        get() = tracks.size

    constructor(name: String) : this() {
        this.name = name
    }

    constructor(name: String, tracks: List<Track>) : this(name) {
        this.name = name
        this.tracks.addAll(tracks)
    }

    constructor(id: Long, name: String, tracks: MutableList<Track>) : this(name) {
        this.id = id
        this.tracks = tracks
    }

    constructor(id: Long, name: String, tracks: MutableList<Track>, favorite: Boolean) : this(name) {
        this.id = id
        this.tracks = tracks
        this.isFavorite = favorite
    }


    fun addTracks(tracks: List<Track>) {
        this.tracks.addAll(tracks)
    }

    @Dao
    interface PlayListDao : BaseDao<PlayList> {
        @get:Query("Select * from playlist")
        val all: List<PlayList>

        @Query("Select * from playlist where id= :id")
        fun getOne(id: Long): PlayList

    }
}
