package binh.com.music.data

/**
 * Created by binh on 2/21/2019.
 */

class Folder(var name: String = "Unknown",
             var path: String? = null,
             var tracks: MutableList<Track> = ArrayList())

