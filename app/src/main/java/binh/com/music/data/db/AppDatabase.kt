package binh.com.music.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

import binh.com.music.data.PlayList
import binh.com.music.data.Track

/**
 * Created by binh on 2/25/2019.
 * app's db management
 */

@Database(entities = [PlayList::class, Track::class, Track.CurrentTrack::class], version = 1)
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun playListDao(): PlayList.PlayListDao
    abstract fun trackDao(): Track.TrackDao
    abstract fun currentTrackDao(): Track.CurrentTrackDao

    companion object {
        private var instance: AppDatabase? = null

        @JvmStatic
        fun getInstance(context: Context): AppDatabase {
            if (instance == null)
                instance = Room.databaseBuilder(context,
                        AppDatabase::class.java, "music").build()
            return instance!!
        }
    }
}
