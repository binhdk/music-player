package binh.com.music.ui.album

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.Cursor
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.PopupMenu
import android.widget.Toast
import binh.com.music.R
import binh.com.music.data.Album
import binh.com.music.ui.playlist.PlaylistChoiceActivity
import binh.com.music.util.IntentUtil
import binh.com.music.util.PersistentDataService
import binh.com.music.util.Util
import kotlinx.android.synthetic.main.fragment_album.*
import java.io.Serializable
import java.util.*

/**
 * Created by binh on 2/22/2019.
 * Display albums
 */

class AlbumFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {
    val ALBUM_LOADER_ID = 3
    private var albums: ArrayList<Album> = ArrayList()
    private lateinit var adapter: AlbumAdapter

    private var storagePermissionGrantedReceiver: BroadcastReceiver? = null

    fun registerReceiver() {
        storagePermissionGrantedReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                loaderManager.initLoader(ALBUM_LOADER_ID, null, this@AlbumFragment)
            }
        }
        val filter1 = IntentFilter(IntentUtil.PERMISSION_STORAGE_GRANTED)

        LocalBroadcastManager.getInstance(context).registerReceiver(storagePermissionGrantedReceiver, filter1)

    }

    fun unregisterReceiver() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(storagePermissionGrantedReceiver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = AlbumAdapter(context, R.layout.item_album, albums)
        registerReceiver()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_album, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        grid.adapter = adapter
        grid.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->
            val album = albums[position]
            val intent = Intent(activity, AlbumDetailActivity::class.java)
            intent.putExtra(IntentUtil.EXTRA_ALBUM, album)
            startActivity(intent)
        }
        grid.onItemLongClickListener = AdapterView.OnItemLongClickListener { parent, v, position, id ->
            showAlbumOptions(v, position)
            true
        }

        swipe.setOnRefreshListener { refresh() }
    }

    private fun refresh() {
        loaderManager.restartLoader(ALBUM_LOADER_ID, null, this)
    }

    private fun showAlbumOptions(v: View, position: Int) {
        val popupMenu = PopupMenu(context, v)
        popupMenu.inflate(R.menu.menu_context_album)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.play -> playAlbum(albums[position])
                R.id.add_to_playlist -> addAlbumToPlaylist(albums[position])
                R.id.delete -> confirmDelete(position)
            }
            true
        }
        popupMenu.show()
    }

    private fun confirmDelete(position: Int) {
        AlertDialog.Builder(context)
                .setMessage("Delete album?")
                .setPositiveButton(resources.getString(R.string.delete)) { dialog, which ->
                    deleteAlbum(position)
                    dialog.dismiss()
                }
                .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which -> dialog.dismiss() }
                .create()
                .show()
    }

    private fun deleteAlbum(position: Int) {
        val album = albums[position]
        albums.removeAt(position)
        adapter.notifyDataSetChanged()


        val albumTracks = Util.getTracks(context,
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Audio.Media.ALBUM_ID + " = " + album.id,
                null,
                MediaStore.Audio.Media.TITLE + " ASC")
        Util.deleteTracksFromDevice(context, albumTracks)


        val intent = Intent(IntentUtil.ACTION_DELETE_TRACKS)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, albumTracks as Serializable)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

        val intent2 = Intent(context, PersistentDataService::class.java)
        intent2.action = IntentUtil.ACTION_DELETE_TRACKS
        intent2.putExtra(IntentUtil.EXTRA_TRACKS, albumTracks as Serializable)
    }

    private fun addAlbumToPlaylist(album: Album) {
        val albumTracks = Util.getTracks(context,
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Audio.Media.ALBUM_ID + " = " + album.id,
                null,
                MediaStore.Audio.Media.TITLE + " ASC")

        val intent = Intent(activity, PlaylistChoiceActivity::class.java)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, albumTracks as Serializable)
        startActivity(intent)
    }

    private fun playAlbum(album: Album) {
        val albumTracks = Util.getTracks(context,
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Audio.Media.ALBUM_ID + " = " + album.id,
                null,
                MediaStore.Audio.Media.TITLE + " ASC")
        if (albumTracks.isEmpty()) {
            Toast.makeText(context, "Cannot play empty album", Toast.LENGTH_SHORT).show()
        } else {
            val intent = Intent(IntentUtil.ACTION_PLAY_ALBUM)
            intent.putExtra(IntentUtil.EXTRA_TRACKS, albumTracks as Serializable)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (Util.checkSelfForStoragePermission(activity)) {
            loaderManager.initLoader(ALBUM_LOADER_ID, null, this)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver()
    }

    private fun updateAlbums(tmp: ArrayList<Album>) {
        albums.clear()
        albums.addAll(tmp)
        adapter.notifyDataSetChanged()
    }


    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val projections = arrayOf(MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM_ART,
                MediaStore.Audio.Albums.NUMBER_OF_SONGS,
                MediaStore.Audio.Albums.ARTIST)
        return CursorLoader(context,
                MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                projections, null, null,
                MediaStore.Audio.Albums.ALBUM + " ASC")
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        val tmp2 = ArrayList<Album>()
        while (data.moveToNext()) {

            val name = data.getString(data.getColumnIndex(MediaStore.Audio.Albums.ALBUM))
            val id = data.getLong(data.getColumnIndex(MediaStore.Audio.Albums._ID))
            val art = data.getString(data.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART))
            val artist = data.getString(data.getColumnIndex(MediaStore.Audio.Albums.ARTIST))
            val numSong = data.getInt(data.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS))
            val album = Album(id = id, name = name, artist = artist, numSong = numSong, art = art)
            tmp2.add(album)
        }
        updateAlbums(tmp2)

        if (swipe.isRefreshing) {
            swipe.isRefreshing = false
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {

    }

    companion object {
        @JvmStatic
        fun newInstance() = AlbumFragment().also {
            val args = Bundle()
            it.arguments = args
        }
    }

}
