package binh.com.music.ui.track

import android.content.Context
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import binh.com.music.R
import binh.com.music.data.Track
import com.bumptech.glide.Glide
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import java.util.*

/**
 * Created by binh on 2/21/2019.
 */

class TrackAdapter(context: Context,
                   resource: Int,
                   private val tracks: List<Track>) : ArrayAdapter<Track>(context, resource, tracks) {
    private val selectedItems: SparseBooleanArray = SparseBooleanArray()
    private var currentSelectedIndex = -1
    private var listener: OnActionButtonClickListener? = null
    var isSelectMulti = false
        set(selectMulti) {
            field = selectMulti
            notifyDataSetChanged()
        }

    val selectedItemCount: Int
        get() = selectedItems.size()


    fun toggleSelection(pos: Int) {
        currentSelectedIndex = pos
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos)
        } else {
            selectedItems.put(pos, true)
        }

        notifyDataSetChanged()
    }


    override fun getCount(): Int {
        return tracks.size
    }

    fun getSelectedItems(): List<Track> {
        val items = ArrayList<Track>(selectedItems.size())
        for (i in 0 until selectedItems.size()) {
            items.add(tracks[selectedItems.keyAt(i)])
        }
        return items
    }

    fun selectAll() {
        for (i in 0 until count)
            selectedItems.put(i, true)
        notifyDataSetChanged()
    }

    fun deSelectAll() {
        selectedItems.clear()
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val viewHolder: ViewHolder
        var newConverterView = convertView
        if (newConverterView != null) {
            viewHolder = newConverterView.tag as ViewHolder
        } else {
            val inflater = LayoutInflater.from(parent.context)
            newConverterView = inflater.inflate(R.layout.item_track, parent, false)
            viewHolder = ViewHolder()
            viewHolder.title = newConverterView.findViewById(R.id.title)
            viewHolder.artist = newConverterView.findViewById(R.id.description)
            viewHolder.image = newConverterView.findViewById(R.id.image)
            viewHolder.addToQueue = newConverterView.findViewById(R.id.add_to_queue)
            viewHolder.more = newConverterView.findViewById(R.id.more)
            viewHolder.checkBox = newConverterView.findViewById(R.id.checkbox)
            newConverterView.tag = viewHolder
        }

        if (!isSelectMulti) {
            viewHolder.addToQueue.visibility = View.VISIBLE
            viewHolder.more.visibility = View.VISIBLE
            viewHolder.checkBox.visibility = View.GONE
        } else {
            viewHolder.addToQueue.visibility = View.GONE
            viewHolder.more.visibility = View.GONE
            viewHolder.checkBox.visibility = View.VISIBLE
        }

        viewHolder.checkBox.isChecked = selectedItems.get(position)

        viewHolder.addToQueue.setOnClickListener {
            listener?.onButtonAddToQueueClick(position)

        }

        viewHolder.more.setOnClickListener {
            listener?.onButtonMoreClick(position)
        }

        val track = getItem(position)
        Glide.with(parent.context)
                .load(track.image)
                .placeholder(R.drawable.ic_track)
                .error(R.drawable.ic_track)
                .transform(RoundedCornersTransformation(
                        context.resources.getInteger(R.integer.rounding_radius_transform),
                        context.resources.getInteger(R.integer.margin_radius_transform)))
                .into(viewHolder.image)
        viewHolder.artist.text = track.artist
        viewHolder.title.text = track.title

        return newConverterView!!
    }

    private fun deSelect(position: Int) {
        selectedItems.delete(position)
    }

    private fun selection(position: Int) {
        selectedItems.put(position, true)
        notifyDataSetChanged()
    }

    fun setListener(listener: OnActionButtonClickListener) {
        this.listener = listener
    }

    class ViewHolder {
        lateinit var image: ImageView
        lateinit var title: TextView
        lateinit var artist: TextView
        lateinit var addToQueue: ImageView
        lateinit var more: ImageView
        lateinit var checkBox: CheckBox
    }

    interface OnActionButtonClickListener {
        fun onButtonAddToQueueClick(position: Int)

        fun onButtonMoreClick(position: Int)
    }
}
