package binh.com.music.ui.playlist

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.SQLException
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import binh.com.music.App
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.PlayList
import binh.com.music.data.Track
import binh.com.music.data.db.AppDatabase
import binh.com.music.util.IntentUtil
import kotlinx.android.synthetic.main.fragment_playlist.*
import java.util.*

/**
 * Created by binh on 2/21/2019.
 * Display playlist
 */

class PlayListFragment : Fragment() {
    private lateinit var playListAdapter: PlayListAdapter
    private lateinit var recyclerPlaylistAdapter: RecyclerPlaylistAdapter
    private var typicalPlaylists: ArrayList<PlayList> = ArrayList()
    private var playLists: ArrayList<PlayList> = ArrayList()
    private var dialog: AlertDialog? = null
    private var playListChangeReceiver: BroadcastReceiver? = null

    fun registerReceivers() {
        playListChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.ACTION_DELETE_PLAYLIST -> {
                            val id = intent.getLongExtra(IntentUtil.EXTRA_PLAYLIST_ID, -1)
                            deletePlaylist(id)
                        }
                        IntentUtil.ACTION_REMOVE_PLAYLIST_TRACK -> {
                            val id = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            deleteTracks(id)
                        }
                        IntentUtil.ACTION_UPDATE_PLAYLIST -> {
                            val playList = intent.getSerializableExtra(IntentUtil.EXTRA_PLAYLIST) as PlayList
                            updatePlayList(playList)
                        }
                        IntentUtil.ACTION_ADD_PLAYLIST -> {
                            val playList = intent.getSerializableExtra(IntentUtil.EXTRA_PLAYLIST) as PlayList
                            addPlaylist(playList)
                        }
                        IntentUtil.ACTION_DELETE_TRACK -> {
                            val mediaId = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            if (mediaId > 0) {
                                deleteTracks(mediaId)
                            }
                        }
                        IntentUtil.ACTION_DELETE_TRACKS -> {
                            val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                            deleteTracks(tracks)
                        }
                    }
                }
            }
        }

        val filter = IntentFilter()
        filter.addAction(IntentUtil.ACTION_DELETE_PLAYLIST)
        filter.addAction(IntentUtil.ACTION_UPDATE_PLAYLIST)
        filter.addAction(IntentUtil.ACTION_ADD_PLAYLIST)
        filter.addAction(IntentUtil.ACTION_DELETE_TRACK)
        filter.addAction(IntentUtil.ACTION_DELETE_TRACKS)
        filter.addAction(IntentUtil.ACTION_REMOVE_PLAYLIST_TRACK)
        LocalBroadcastManager.getInstance(context).registerReceiver(playListChangeReceiver, filter)
    }

    private fun deleteTracks(tracks: List<Track>) {
        for (track in tracks) {
            deleteTracks(track.id)
        }

    }

    private fun deleteTracks(mediaId: Long) {
        for (playList in playLists) {
            val removes = ArrayList<Track>()
            for (track in playList.tracks) {
                if (track.id == mediaId) {
                    removes.add(track)
                }
            }
            playList.tracks.removeAll(removes)
            updatePlayList(playList)
        }
    }

    private fun addPlaylist(playList: PlayList) {
        for (tmp in playLists) {
            if (tmp.id == playList.id) {
                updatePlayList(playList)
                playListAdapter.notifyDataSetChanged()
                return
            }
        }

        playLists.add(playList)
        playListAdapter.notifyDataSetChanged()

    }

    private fun updatePlayList(playList: PlayList) {
        var position = -1
        for (index in playLists.indices) {
            if (playLists[index].id == playList.id) {
                position = index
            }
        }

        if (position >= 0) {
            playLists[position] = playList
            playListAdapter.notifyDataSetChanged()

            Thread(Runnable {
                try {
                    AppDatabase.getInstance(App.context).playListDao().update(playList)
                } catch (e: SQLException) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }
            }).start()
        }
    }

    private fun deletePlaylist(id: Long) {
        val deletes = ArrayList<PlayList>()
        for (item in playLists) {
            if (item.id == id) {
                deletes.add(item)

            }
        }

        playLists.removeAll(deletes)
        playListAdapter.notifyDataSetChanged()

        Thread(Runnable {
            try {
                AppDatabase.getInstance(App.context).playListDao().delete(deletes)
            } catch (e: SQLException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        playLists = ArrayList()
        typicalPlaylists = ArrayList()
        playListAdapter = PlayListAdapter(context, R.layout.item_playlist, playLists)
        recyclerPlaylistAdapter = RecyclerPlaylistAdapter(typicalPlaylists)

        getTypicalPlayLists()
        getPlayLists()

        registerReceivers()
    }

    private fun getTypicalPlayLists() {

        if (isVisible && swipe_playlist.isRefreshing) {
            swipe_playlist.isRefreshing = false
        }
    }

    private fun getPlayLists() {
        Thread(Runnable {
            try {
                val tmp = AppDatabase.getInstance(context)
                        .playListDao()
                        .all
                if (isAdded) {
                    activity.runOnUiThread { updatePlayLists(tmp) }
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }

            if (isVisible && swipe_playlist.isRefreshing) {
                swipe_playlist.isRefreshing = false
            }
        }).start()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_playlist, container, false)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe_playlist.setOnRefreshListener { refresh() }

        list.adapter = playListAdapter
        list.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->
            val intent = Intent(activity, PlayListDetailActivity::class.java)
            intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playLists[position])
            startActivity(intent)
        }

        list.onItemLongClickListener = AdapterView.OnItemLongClickListener { parent, v, position, id ->
            showBottomSheet(position)
            true
        }


        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recycler.adapter = recyclerPlaylistAdapter

        recyclerPlaylistAdapter.setListener(object : RecyclerPlaylistAdapter.OnItemViewClickListener {
            override fun onClick(position: Int) {
                val intent = Intent(activity, PlayListDetailActivity::class.java)
                intent.putExtra(IntentUtil.EXTRA_PLAYLIST, typicalPlaylists[position])
                startActivity(intent)
            }
        })

        add_playlist.setOnClickListener { showDialogAddPlaylist() }


    }

    private fun showBottomSheet(position: Int) {
        val fragment = PlayListBottomSheetFragment.newInstance(playLists[position])
        fragment.show(fragmentManager, fragment.tag)
    }

    private fun refresh() {
        getPlayLists()
        getTypicalPlayLists()
    }

    private fun showDialogAddPlaylist() {
        dialog = AlertDialog.Builder(activity)
                .setTitle("Create playlist")
                .setPositiveButton("OK") { dialog, which ->
                    val playlistName = this@PlayListFragment.dialog!!.findViewById<EditText>(R.id.playlist_name)
                    addPlaylist(playlistName.text.toString())
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }
                .create()
        val view = layoutInflater.inflate(R.layout.dialog_add_playlist, null)
        dialog!!.setView(view)
        dialog!!.show()
        val playlistName = dialog!!.findViewById<EditText>(R.id.playlist_name)
        playlistName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (TextUtils.isEmpty(s)) {
                    playlistName.error = "None empty name"
                } else {
                    playlistName.error = null
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }

        unregisterReceivers()
    }

    private fun unregisterReceivers() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(playListChangeReceiver)
    }

    private fun addPlaylist(text: String) {
        if (!TextUtils.isEmpty(text)) {
            val playList = PlayList(text)
            Thread(Runnable {
                val id = AppDatabase.getInstance(App.context).playListDao().insert(playList)
                if (isAdded && id >= 0) {
                    activity.runOnUiThread {
                        playList.id = id
                        playLists.add(playList)
                        playListAdapter.notifyDataSetChanged()
                    }
                }
            }).start()
        }
    }

    private fun updatePlayLists(tmp: List<PlayList>) {
        playLists.clear()
        playLists.addAll(tmp)
        playListAdapter.notifyDataSetChanged()
    }

    companion object {

        fun newInstance(): PlayListFragment {

            val args = Bundle()
            val fragment = PlayListFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
