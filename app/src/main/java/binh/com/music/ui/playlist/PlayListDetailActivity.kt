package binh.com.music.ui.playlist

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.SQLException
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.SpannableString
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.PopupMenu
import binh.com.music.App
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.PlayList
import binh.com.music.data.Track
import binh.com.music.data.db.AppDatabase
import binh.com.music.ui.track.TrackAdapter
import binh.com.music.ui.track.TrackBottomSheetFragment
import binh.com.music.ui.track.TrackSelectionActivity
import binh.com.music.util.IntentUtil
import binh.com.music.util.Playback
import binh.com.music.util.Sort
import kotlinx.android.synthetic.main.activity_play_list_detail.*
import java.util.*

class PlayListDetailActivity : AppCompatActivity() {
    val SELECT_TRACK_REQUEST_CODE = 1
    private lateinit var playList: PlayList
    private lateinit var adapter: TrackAdapter
    private var dialog: AlertDialog? = null
    private val clickListener = View.OnClickListener { v ->
        when (v.id) {
            R.id.play -> addPlaylistToQueue(false, 0)
            R.id.shuffle -> addPlaylistToQueue(true, 0)
            R.id.sort -> showSortOptions(v)
        }
    }
    private var playListChangeReceiver: BroadcastReceiver? = null

    fun registerReceivers() {
        playListChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {

                        IntentUtil.ACTION_REMOVE_PLAYLIST_TRACK -> {
                            val id = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            if (id >= 0) {
                                removeTracks(id)
                            }
                        }
                    }
                }
            }
        }

        val filter = IntentFilter()
        filter.addAction(IntentUtil.ACTION_REMOVE_PLAYLIST_TRACK)
        LocalBroadcastManager.getInstance(this).registerReceiver(playListChangeReceiver, filter)
    }

    private fun unregisterReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(playListChangeReceiver)
    }

    private fun removeTracks(id: Long) {
        val removes = ArrayList<Track>()
        for (track in playList.tracks) {
            if (track.id == id) {
                removes.add(track)
            }
        }
        playList.tracks.removeAll(removes)
        adapter.notifyDataSetChanged()
        Thread(Runnable {
            try {
                AppDatabase.getInstance(App.context).playListDao().update(playList)
            } catch (e: SQLException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()

        count.text = playList.numTracks.toString() + if (playList.numTracks > 1) " tracks" else " track"
    }


    private fun showSortOptions(v: View) {
        val popupMenu = PopupMenu(this, v)
        popupMenu.inflate(R.menu.menu_sort_playlist_track)
        val sortName = popupMenu.menu.findItem(R.id.sort_name)
        val sortArtist = popupMenu.menu.findItem(R.id.sort_artist)

        val sort = PreferenceManager.getDefaultSharedPreferences(this)
                .getInt("playlist-" + playList.id, Sort.NAME)
        if (sort == Sort.ARTIST) {
            val s = SpannableString(sortArtist.title)
            s.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 0, s.length, 0)
            sortArtist.title = s
        } else {
            val s = SpannableString(sortName.title)
            s.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 0, s.length, 0)
            sortName.title = s
        }
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.sort_name -> sortByName()
                R.id.sort_artist -> sortByArtist()
            }
            true
        }
        popupMenu.show()
    }

    private fun sortByArtist() {
        playList.tracks.sortWith(Comparator { o1, o2 -> o1.artist.compareTo(o2.artist) })

        adapter.notifyDataSetChanged()
        persistPlaylist()
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putInt("playlist-" + playList.id, Sort.ARTIST)
                .apply()
    }

    private fun sortByName() {
        playList.tracks.sortWith(Comparator { o1, o2 -> o1.title.compareTo(o2.title) })

        adapter.notifyDataSetChanged()
        persistPlaylist()
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putInt("playlist-" + playList.id, Sort.NAME)
                .apply()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_list_detail)

        val tmp = intent.getSerializableExtra(IntentUtil.EXTRA_PLAYLIST) as? PlayList
        if (tmp == null) {
            onBackPressed()
        } else {
            playList = tmp
        }


        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = playList.name

        count.text = playList.numTracks.toString() + if (playList.numTracks > 1) " tracks" else " track"


        play.setOnClickListener(clickListener)
        sort.setOnClickListener(clickListener)
        shuffle.setOnClickListener(clickListener)


        adapter = TrackAdapter(this, R.layout.item_track, playList.tracks)

        adapter.setListener(object : TrackAdapter.OnActionButtonClickListener {
            override fun onButtonAddToQueueClick(position: Int) {
                addTrackToQueue(position)
            }

            override fun onButtonMoreClick(position: Int) {
                showBottomSheet(position)
            }
        })
        list.adapter = adapter
        list.onItemClickListener =
                AdapterView.OnItemClickListener { parent, view, position, id -> addPlaylistToQueue(false, position) }

        registerReceivers()

    }

    private fun showBottomSheet(position: Int) {
        val fragment = TrackBottomSheetFragment.newInstance(playList.tracks[position], true)
        fragment.show(supportFragmentManager, fragment.tag)
    }

    private fun addTrackToQueue(position: Int) {
        val intent = Intent(IntentUtil.ACTION_ADD_QUEUE_ITEM)
        intent.putExtra(IntentUtil.EXTRA_TRACK, playList.tracks[position])
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (dialog?.isShowing == true)
            dialog?.dismiss()
        unregisterReceivers()
    }

    private fun addPlaylistToQueue(shuffle: Boolean, lastPlayPosition: Int) {
        if (shuffle) {
            val intent = Intent(Playback.ACTION_SHUFFLE)
            intent.putExtra(Playback.EXTRA_SHUFFLE, true)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        }

        playList.tracks.forEach { it.isLastPlay = false }

        playList.tracks[lastPlayPosition].isLastPlay = true

        val intent = Intent(IntentUtil.ACTION_PLAY_PLAYLIST)
        intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_playlist_detail, menu)
        val addToFavorite = menu.findItem(R.id.add_to_favorites)
        val removeFromFavorite = menu.findItem(R.id.remove_from_favorites)

        if (playList.isFavorite) {
            removeFromFavorite.isVisible = true
            addToFavorite.isVisible = false
        } else {
            removeFromFavorite.isVisible = false
            addToFavorite.isVisible = true
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_to_favorites -> changeFavorites(true)
            R.id.add_track -> addTrackToPlaylist()
            R.id.rename -> showDialogRename()
            R.id.remove_from_favorites -> changeFavorites(false)
        }

        return super.onOptionsItemSelected(item)
    }


    private fun showDialogRename() {
        dialog = AlertDialog.Builder(this)
                .setTitle("Rename playlist")
                .setPositiveButton("OK") { dialog, which ->
                    val playlistName = this@PlayListDetailActivity.dialog!!.findViewById<EditText>(R.id.playlist_name)
                    updatePlaylist(playlistName!!.text.toString())
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }
                .create()
        val view = layoutInflater.inflate(R.layout.dialog_add_playlist, null)
        dialog!!.setView(view)
        dialog!!.show()
        val playlistName = dialog!!.findViewById<EditText>(R.id.playlist_name)
        playlistName!!.setText(playList.name)
        playlistName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (TextUtils.isEmpty(s)) {
                    playlistName.error = "None empty name"
                } else {
                    playlistName.error = null
                }
            }
        })
    }

    private fun updatePlaylist(name: String) {
        if (!TextUtils.isEmpty(name)) {
            playList.name = name
            val intent = Intent(IntentUtil.ACTION_UPDATE_PLAYLIST)
            intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            title = playList.name
            persistPlaylist()
        }

    }

    private fun addTrackToPlaylist() {
        startActivityForResult(Intent(this@PlayListDetailActivity, TrackSelectionActivity::class.java), SELECT_TRACK_REQUEST_CODE)
    }

    private fun changeFavorites(favorite: Boolean) {
        playList.isFavorite = favorite
        val intent = Intent(IntentUtil.ACTION_UPDATE_PLAYLIST)
        intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

        invalidateOptionsMenu()
        persistPlaylist()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SELECT_TRACK_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    val selectedTracks = data.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                    onTracksSelected(selectedTracks)

                } catch (e: ClassCastException) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }

            }
        }
    }

    private fun onTracksSelected(selectedTracks: List<Track>) {
        playList.tracks.addAll(selectedTracks)
        adapter.notifyDataSetChanged()
        count.text = playList.numTracks.toString() + if (playList.numTracks > 1) " tracks" else " track"

        val intent = Intent(IntentUtil.ACTION_UPDATE_PLAYLIST)
        intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

        persistPlaylist()

    }

    private fun persistPlaylist() {
        Thread(Runnable {
            try {
                AppDatabase.getInstance(this@PlayListDetailActivity).playListDao().update(playList)
            } catch (e: SQLException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()

    }

}
