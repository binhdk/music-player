package binh.com.music.ui.track

import android.content.*
import android.database.Cursor
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.view.*
import android.widget.AbsListView
import android.widget.AdapterView
import binh.com.music.R
import binh.com.music.data.Track
import binh.com.music.ui.playlist.PlaylistChoiceActivity
import binh.com.music.util.Constant
import binh.com.music.util.IntentUtil
import binh.com.music.util.PersistentDataService
import binh.com.music.util.Util
import kotlinx.android.synthetic.main.fragment_track.*
import java.io.Serializable
import java.util.*


/**
 * Created by binh on 2/21/2019.
 * Display Tracks
 */

class TrackFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {
    private var actionMode: ActionMode? = null
    private var isSelectedAll = false

    private val actionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            mode.menuInflater.inflate(R.menu.menu_track_action_mode, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.delete -> {
                    confirmDelete(adapter.getSelectedItems())
                    dismissActionMode()
                }
                R.id.add_to_queue -> {
                    addToQueue(adapter.getSelectedItems())
                    dismissActionMode()
                }
                R.id.add_to_playlist -> {
                    addToPlaylist(adapter.getSelectedItems())
                    dismissActionMode()
                }
                R.id.select_deselect_all -> selectDeselectAll()
            }
            return true
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            listView!!.clearChoices()
            adapter.deSelectAll()
            adapter.isSelectMulti = false
            actionMode = null
            isSelectedAll = false
        }
    }
    private var mediaChangeReceiver: BroadcastReceiver? = null
    private lateinit var adapter: TrackAdapter
    private var tracks: ArrayList<Track> = ArrayList()
    private var storagePermissionGrantedReceiver: BroadcastReceiver? = null

    private fun selectDeselectAll() {
        if (adapter.selectedItemCount == 0) {
            isSelectedAll = true
            adapter.selectAll()
        } else if (adapter.selectedItemCount == adapter.count) {
            adapter.deSelectAll()
        } else if (isSelectedAll) {
            isSelectedAll = false
            adapter.deSelectAll()
        } else {
            isSelectedAll = true
            adapter.selectAll()
        }

        val count = adapter.selectedItemCount
        if (count == 0) {
            dismissActionMode()
        } else {
            actionMode!!.title = count.toString()
            actionMode!!.invalidate()
        }
    }

    private fun addToPlaylist(selectedItems: List<Track>) {
        val intent = Intent(activity, PlaylistChoiceActivity::class.java)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, selectedItems as Serializable)
        startActivity(intent)
    }

    private fun confirmDelete(selectedItems: List<Track>) {
        AlertDialog.Builder(context)
                .setMessage("Delete these items?")
                .setNegativeButton(getString(R.string.cancel)) { dialog, which -> dialog.dismiss() }
                .setPositiveButton(getString(R.string.delete)) { dialog, which ->
                    dialog.dismiss()
                    dismissActionMode()
                    delete(selectedItems)
                }
                .create()
                .show()

    }

    private fun delete(tracks: List<Track>) {
        Util.deleteTracksFromDevice(context, tracks)
        val intent = Intent(IntentUtil.ACTION_DELETE_TRACKS)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

        val intent2 = Intent(context, PersistentDataService::class.java)
        intent2.action = IntentUtil.ACTION_DELETE_TRACKS
        intent2.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable)
    }

    private fun addToQueue(selectedItems: List<Track>) {
        val intent = Intent(IntentUtil.ACTION_ADD_QUEUE_ITEMS)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, selectedItems as ArrayList<*>)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    private fun addToQueue(track: Track) {
        val intent = Intent(IntentUtil.ACTION_ADD_QUEUE_ITEM)
        intent.putExtra(IntentUtil.EXTRA_TRACK, track)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    fun registerReceiver() {
        storagePermissionGrantedReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                loaderManager.initLoader(TRACK_LOADER_ID, null, this@TrackFragment)
            }
        }
        val filter1 = IntentFilter(IntentUtil.PERMISSION_STORAGE_GRANTED)

        LocalBroadcastManager.getInstance(context).registerReceiver(storagePermissionGrantedReceiver, filter1)
        mediaChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.ACTION_ADD_TO_FAVORITE -> {
                            val mediaId = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            val favorite = intent.getBooleanExtra(IntentUtil.EXTRA_FAVORITE, false)
                            if (mediaId > 0)
                                addMediaToFavorite(mediaId,favorite)
                        }
                        IntentUtil.ACTION_DELETE_TRACK -> {
                            val mediaId = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            if (mediaId > 0) {
                                deleteTracks(mediaId)
                            }
                        }
                        IntentUtil.ACTION_DELETE_TRACKS -> {
                            val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                            deleteTracks(tracks)
                        }
                    }
                }
            }
        }

        val filter2 = IntentFilter()
        filter2.addAction(IntentUtil.ACTION_ADD_TO_FAVORITE)
        filter2.addAction(IntentUtil.ACTION_DELETE_TRACK)
        filter2.addAction(IntentUtil.ACTION_DELETE_TRACKS)

        LocalBroadcastManager.getInstance(context).registerReceiver(mediaChangeReceiver, filter2)

    }

    private fun deleteTracks(tracks: List<Track>) {
        val deletes = ArrayList<Track>()
        for (track in tracks) {
            for (q in this.tracks) {
                if (track.id == q.id) {
                    deletes.add(q)
                }
            }
        }

        this.tracks.removeAll(deletes)
        adapter.notifyDataSetChanged()
    }

    private fun deleteTracks(mediaId: Long) {
        val removes = ArrayList<Track>()
        for (track in tracks) {
            if (track.id == mediaId) {
                removes.add(track)
            }
        }

        tracks.removeAll(removes)
        adapter.notifyDataSetChanged()
    }

    fun unregisterReceiver() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(storagePermissionGrantedReceiver)
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mediaChangeReceiver)
    }

    private fun addMediaToFavorite(mediaId: Long,favorite:Boolean) {
        for (track in tracks) {
            if (mediaId == track.id) {
                track.isFavorite = favorite
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerReceiver()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_track, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = TrackAdapter(context, R.layout.item_track, tracks)
        adapter.setListener(object : TrackAdapter.OnActionButtonClickListener {
            override fun onButtonAddToQueueClick(position: Int) {
                addToQueue(tracks[position])
            }

            override fun onButtonMoreClick(position: Int) {
                showActionMore(tracks[position])
            }
        })
        listView!!.adapter = adapter
        listView!!.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->
            if (actionMode != null) {
                toggleSelection(position)
                return@OnItemClickListener
            }
            val track = parent.getItemAtPosition(position) as Track
            playItem(track)
        }
        listView!!.choiceMode = AbsListView.CHOICE_MODE_MULTIPLE

        listView!!.onItemLongClickListener = AdapterView.OnItemLongClickListener { parent, v, position, id ->
            toggleSelection(position)
            adapter.isSelectMulti = true
            true
        }


        swipe.setOnRefreshListener { refresh() }

    }

    private fun refresh() {
        loaderManager.restartLoader(TRACK_LOADER_ID, null, this)
    }

    private fun showActionMore(track: Track) {
        val bottomSheetFragment = TrackBottomSheetFragment.newInstance(track)
        bottomSheetFragment.show(fragmentManager, bottomSheetFragment.tag)
    }


    private fun playItem(track: Track) {
        val intent = Intent(IntentUtil.ACTION_PLAY_QUEUE_ITEM)
        intent.putExtra(IntentUtil.EXTRA_TRACK, track)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    private fun enableActionMode() {
        if (actionMode == null)
            actionMode = (activity as AppCompatActivity).startSupportActionMode(actionModeCallback)
    }

    private fun dismissActionMode() {
        actionMode?.finish()
    }

    private fun toggleSelection(position: Int) {
        enableActionMode()
        adapter.toggleSelection(position)

        val count = adapter.selectedItemCount
        if (count == 0) {
            dismissActionMode()
        } else {
            actionMode?.title = count.toString()
            actionMode?.invalidate()
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (Util.checkSelfForStoragePermission(activity))
            loaderManager.initLoader(TRACK_LOADER_ID, null, this)

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        // Make sure that we are currently visible
        if (this.isVisible) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                dismissActionMode()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver()
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val projections = arrayOf(MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.IS_RINGTONE,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ARTIST_ID,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATA)
        return CursorLoader(context,
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projections, null, null,
                MediaStore.Audio.Media.TITLE + " ASC")
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        val tmp = ArrayList<Track>()
        while (data.moveToNext()) {

            val id = data.getLong(data.getColumnIndex(MediaStore.Audio.Media._ID))
            val title = data.getString(data.getColumnIndex(MediaStore.Audio.Media.TITLE))
            val artist = data.getString(data.getColumnIndex(MediaStore.Audio.Media.ARTIST))
            val artistId = data.getLong(data.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID))
            val album = data.getString(data.getColumnIndex(MediaStore.Audio.Media.ALBUM))
            val albumId = data.getLong(data.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))
            val duration = data.getLong(data.getColumnIndex(MediaStore.Audio.Media.DURATION))
            val ringtone = data.getInt(data.getColumnIndex(MediaStore.Audio.Media.IS_RINGTONE))
            val path = data.getString(data.getColumnIndex(MediaStore.Audio.Media.DATA))

            val uri = ContentUris.withAppendedId(Constant.albumartUri, albumId)
            val image = uri.toString()
            val track = Track(id = id, title = title, path = path, artistId = artistId, artist = artist,
                    albumId = albumId, album = album, duration = duration, ringtone = ringtone, image = image)
            tmp.add(track)
        }
        updateTracks(tmp)

        if (swipe.isRefreshing) {
            swipe.isRefreshing = false
        }

    }

    override fun onLoaderReset(loader: Loader<Cursor>) {

    }


    private fun updateTracks(tmp: ArrayList<Track>) {
        tracks.clear()
        tracks.addAll(tmp)
        adapter.notifyDataSetChanged()

    }

    companion object {
        private const val TRACK_LOADER_ID = 1

        @JvmStatic
        fun newInstance(): TrackFragment = TrackFragment().also {
            val args = Bundle()
            it.arguments = args
        }
    }
}
