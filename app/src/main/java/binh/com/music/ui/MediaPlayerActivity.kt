package binh.com.music.ui

import android.app.Dialog
import android.content.*
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import binh.com.music.App
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.Track
import binh.com.music.data.db.AppDatabase
import binh.com.music.util.IntentUtil
import binh.com.music.util.MediaPlayerService
import binh.com.music.util.Util
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_media_player.*
import kotlinx.android.synthetic.main.activity_media_player.title as trackTitle

class MediaPlayerActivity : AppCompatActivity(), View.OnClickListener {

    private var service: MediaPlayerService? = null
    private var bound = false
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MediaPlayerService.LocalBinder
            this@MediaPlayerActivity.service = binder.service
            bound = true
            updateUI(this@MediaPlayerActivity.service!!.playbackStateCompat,
                    this@MediaPlayerActivity.service!!.metadata)

            actionButtonHandler.post(actionButtonRunnable)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            bound = false
            updateUI(this@MediaPlayerActivity.service!!.playbackStateCompat,
                    this@MediaPlayerActivity.service!!.metadata)
            actionButtonHandler.removeCallbacks(actionButtonRunnable)
        }
    }

    private val seekBarHandler = Handler()
    private val seekBarRunnable = object : Runnable {
        override fun run() {
            if (isServiceBound && service!!.isPrepared) {
                seek_bar.max = (service!!.duration / 1000).toInt()
                seek_bar.progress = service!!.currentPosition / 1000
                seekBarHandler.postDelayed(this, 1000)

                total.text = Util.formatMillisecondToMMSS(service!!.duration)
                progress.text = Util.formatMillisecondToMMSS(service!!.currentPosition.toLong())

            }
        }
    }

    private val actionButtonHandler = Handler()
    private val actionButtonRunnable = object : Runnable {
        override fun run() {
            if (isServiceBound) {
                updateActionButton()
                actionButtonHandler.postDelayed(this, 1000)
            }
        }
    }
    private var audioManager: AudioManager? = null

    private var playbackReceiver: BroadcastReceiver? = null

    val isServiceBound: Boolean
        get() = bound && service != null

    private val seekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            seekBar.progress = progress
            if (isServiceBound && fromUser) {
                if (service!!.isPrepared) {
                    service!!.seekTo(progress * 1000)
                }
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {

        }
    }

    private fun updateActionButton() {
        if (isServiceBound) {
            if (service!!.isShuffle) {
                no_shuffle.visibility = View.VISIBLE
                shuffle.visibility = View.GONE
            } else {
                no_shuffle.visibility = View.GONE
                shuffle.visibility = View.VISIBLE
            }

            val loop = service!!.loop
            updateLoopButton(loop)
        }
    }

    fun updateSeekBar() {
        if (isServiceBound && service!!.isPrepared) {
            seekBarHandler.post(seekBarRunnable)
        } else {
            resetSeekBar()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media_player)

        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_down)
        }

        this.title = ""

        seek_bar.let {
            it.progressDrawable.colorFilter = PorterDuffColorFilter(
                    ContextCompat.getColor(this, android.R.color.white),
                    PorterDuff.Mode.MULTIPLY)
            it.thumb.setColorFilter(ContextCompat.getColor(this, R.color.seekbarColor), PorterDuff.Mode.SRC_IN)
            it.setOnSeekBarChangeListener(seekBarChangeListener)
        }


        prev.setOnClickListener(this)
        next.setOnClickListener(this)
        play.setOnClickListener(this)
        pause.setOnClickListener(this)
        shuffle.setOnClickListener(this)
        no_shuffle.setOnClickListener(this)
        no_loop.setOnClickListener(this)
        loop_one.setOnClickListener(this)
        loop_all.setOnClickListener(this)
        add_to_favorites.setOnClickListener(this)
        remove_from_favorites.setOnClickListener(this)
        queue.setOnClickListener(this)
        more.setOnClickListener(this)

        //get audio manager
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        volumeControlStream = AudioManager.STREAM_MUSIC

        registerReceivers()

        val intent = Intent().also { it.setClass(this, MediaPlayerService::class.java) }
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }

    fun updateUI(playbackStateCompat: PlaybackStateCompat?, metadata: MediaMetadataCompat?) {
        updateSeekBar()
        if (metadata != null) {
            Glide.with(this)
                    .load(metadata.description.iconUri)
                    .error(R.drawable.ic_placeholder)
                    .placeholder(R.drawable.ic_placeholder)
                    .circleCrop()
                    .into(image!!)
            trackTitle.text = metadata.description.title
            artist.text = metadata.description.subtitle

            //check is favorite
            Thread(Runnable {
                try {
                    val mediaId = java.lang.Long.parseLong(metadata.description.mediaId)
                    val track:Track? = AppDatabase.getInstance(App.context).trackDao().getById(mediaId)
                    if(track!=null) {
                        runOnUiThread { updateFavoriteUI(track.isFavorite) }
                    }
                } catch (e: Exception) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }
            }).start()
        } else {
            image.setImageResource(R.drawable.ic_placeholder)
            trackTitle.text = "Empty queue"
            artist.text = ""
            seekBarHandler.removeCallbacks(seekBarRunnable)
            seek_bar.progress = 0
        }

        if (playbackStateCompat != null && playbackStateCompat.state == PlaybackStateCompat.STATE_PLAYING) {
            pause.visibility = View.VISIBLE
            play.visibility = View.GONE
        } else {
            pause.visibility = View.GONE
            play.visibility = View.VISIBLE
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.effect_do_nothing, R.anim.effect_push_out_bottom)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        // Unbind from the service
        if (bound) {
            unbindService(connection)
            bound = false
        }

        unregisterReceivers()
        seekBarHandler.removeCallbacks(seekBarRunnable)
        actionButtonHandler.removeCallbacks(actionButtonRunnable)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_media_player, menu)

        val menuItem = menu.findItem(R.id.timer)
        if (isServiceBound) {
            if (service!!.isEnableTimer) {
                menuItem.setIcon(R.drawable.ic_timer_enabled)
            } else {
                menuItem.setIcon(R.drawable.ic_timer)
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.volume -> showAdjustVolume()
            R.id.timer -> showDialogSetTimer()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showDialogSetTimer() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.layout_timer)
        dialog.show()
        val aSwitch = dialog.findViewById<Switch>(R.id.enable_timer)
        if (isServiceBound && service!!.isEnableTimer) {
            aSwitch.isChecked = true
        }

        val radioGroup = dialog.findViewById<RadioGroup>(R.id.group_timer)
        val inputTimer = dialog.findViewById<EditText>(R.id.time_input)

        val time = dialog.findViewById<TextView>(R.id.time)
        if (isServiceBound && service!!.isEnableTimer) {
            time.text = Util.formatMillisecondToHHMM(service!!.timerUntilFinished)
        } else {
            time.text = ""
        }

        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            var timer = (30 * 60 * 1000).toLong()
            when (checkedId) {
                R.id.radio60 -> timer = (60 * 60 * 1000).toLong()
                R.id.radio90 -> timer = (90 * 60 * 1000).toLong()
                R.id.radio120 -> timer = (120 * 60 * 1000).toLong()
            }
            if (isServiceBound) {
                aSwitch.isChecked = true
                service!!.startTimer(timer)
                time.text = Util.formatMillisecondToHHMM(service!!.timerUntilFinished)
                invalidateOptionsMenu()
            }
        }

        aSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isServiceBound) {
                if (isChecked) {
                    var timer = (30 * 60 * 1000).toLong()
                    if (!TextUtils.isEmpty(inputTimer.text)) {
                        timer = java.lang.Long.parseLong(inputTimer.text.toString()) * 60 * 1000
                    } else {
                        when (radioGroup.checkedRadioButtonId) {
                            R.id.radio60 -> timer = (60 * 60 * 1000).toLong()
                            R.id.radio90 -> timer = (90 * 60 * 1000).toLong()
                            R.id.radio120 -> timer = (120 * 60 * 1000).toLong()
                            R.id.radio30 -> (dialog.findViewById<View>(R.id.radio30) as RadioButton).isChecked = true
                            else -> (dialog.findViewById<View>(R.id.radio30) as RadioButton).isChecked = true
                        }
                    }
                    service?.startTimer(timer)
                    time.text = Util.formatMillisecondToHHMM(service!!.timerUntilFinished)
                } else {
                    service?.stopTimer()
                    time.text = ""
                }
                invalidateOptionsMenu()
            }
        }

    }

    private fun showAdjustVolume() {
        audioManager?.adjustVolume(AudioManager.ADJUST_SAME, AudioManager.FLAG_SHOW_UI)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.play -> play()
            R.id.pause -> pause()
            R.id.next -> {
                resetSeekBar()
                next()
            }
            R.id.prev -> {
                resetSeekBar()
                prev()
            }
            R.id.media_layout -> {
                startActivity(Intent(this@MediaPlayerActivity, MediaPlayerActivity::class.java))
                overridePendingTransition(R.anim.effect_pull_in_top, R.anim.effect_do_nothing)
            }
            R.id.no_loop -> setLoop(MediaPlayerService.LOOP_ONE)
            R.id.loop_all -> setLoop(MediaPlayerService.NO_LOOP)
            R.id.loop_one -> setLoop(MediaPlayerService.LOOP_ALL)
            R.id.shuffle -> setShuffle(true)
            R.id.no_shuffle -> setShuffle(false)
            R.id.add_to_favorites -> setFavorite(true)
            R.id.remove_from_favorites -> setFavorite(false)

            R.id.more -> showMoreAction()

            R.id.queue -> startActivity(Intent(this@MediaPlayerActivity, QueueActivity::class.java))
        }
    }

    private fun showMoreAction() {

    }

    private fun setFavorite(favorite: Boolean) {
        if (isServiceBound) {
            if (service?.metadata == null)
                return

            val mediaId = java.lang.Long.parseLong(service!!.metadata!!.description.mediaId)
            Thread(Runnable {
                try {
                    val track = AppDatabase.getInstance(App.context).trackDao().getById(mediaId)
                    track.isFavorite = favorite
                    val row = AppDatabase.getInstance(App.context).trackDao().update(track)
                    if (row == 1) {
                        runOnUiThread {
                            updateFavoriteUI(favorite)
                            val intent = Intent(IntentUtil.ACTION_ADD_TO_FAVORITE)
                            intent.putExtra(IntentUtil.EXTRA_TRACK_ID, mediaId)
                            intent.putExtra(IntentUtil.EXTRA_FAVORITE, favorite)
                            LocalBroadcastManager.getInstance(App.context).sendBroadcast(intent)
                        }
                    }
                } catch (e: Exception) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }
            }).start()
        }
    }

    private fun updateFavoriteUI(favorite: Boolean) {
        if (favorite) {
            add_to_favorites.visibility = View.GONE
            remove_from_favorites.visibility = View.VISIBLE
        } else {
            add_to_favorites.visibility = View.VISIBLE
            remove_from_favorites.visibility = View.GONE
        }
    }

    private fun setLoop(loopType: Int) {
        if (isServiceBound) {
            service?.loop = loopType
            updateLoopButton(loopType)

        }
    }

    private fun updateLoopButton(loopType: Int) {
        when (loopType) {
            MediaPlayerService.NO_LOOP -> {
                no_loop.visibility = View.VISIBLE
                loop_one.visibility = View.GONE
                loop_all.visibility = View.GONE
            }
            MediaPlayerService.LOOP_ONE -> {
                no_loop.visibility = View.GONE
                loop_one.visibility = View.VISIBLE
                loop_all.visibility = View.GONE
            }
            MediaPlayerService.LOOP_ALL -> {
                no_loop.visibility = View.GONE
                loop_one.visibility = View.GONE
                loop_all.visibility = View.VISIBLE
            }
        }
    }

    private fun setShuffle(shuffle: Boolean) {
        if (isServiceBound) {
            service!!.isShuffle = shuffle
        }
    }

    fun registerReceivers() {
        playbackReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.PLAYBACK_STATE_CHANGE -> {
                            val state = intent.getParcelableExtra<PlaybackStateCompat>("playbackstate")
                            val metadata = intent.getParcelableExtra<MediaMetadataCompat>("metadata")
                            updateUI(state, metadata)
                        }
                        IntentUtil.ACTION_TIMER_FINISH -> invalidateOptionsMenu()
                    }
                }
            }
        }

        val filter1 = IntentFilter()
        filter1.addAction(IntentUtil.PLAYBACK_STATE_CHANGE)
        filter1.addAction(IntentUtil.ACTION_TIMER_FINISH)

        LocalBroadcastManager.getInstance(this).registerReceiver(playbackReceiver, filter1)
    }

    private fun resetSeekBar() {
        seek_bar.progress = 0
        total.setText(R.string.time_init)
        progress.setText(R.string.time_init)
        seekBarHandler.removeCallbacks(seekBarRunnable)
    }

    fun unregisterReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(playbackReceiver)
    }

    private fun prev() {
        if (isServiceBound)
            service?.previous()
    }

    private operator fun next() {
        if (isServiceBound)
            service?.next()
    }

    private fun pause() {
        if (isServiceBound)
            service?.pause()
    }

    private fun play() {
        if (isServiceBound) {
            service?.play()
        }
    }
}
