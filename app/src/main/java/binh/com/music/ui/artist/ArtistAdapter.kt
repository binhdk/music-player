package binh.com.music.ui.artist

import android.content.Context
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide

import java.util.ArrayList

import binh.com.music.R
import binh.com.music.data.Artist
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

/**
 * Created by binh on 2/23/2019.
 */

class ArtistAdapter(context: Context, resource: Int, private val artists: List<Artist>) : ArrayAdapter<Artist>(context, resource, artists) {
    private val selectedItems: SparseBooleanArray = SparseBooleanArray()
    private var selectMulti = false

    val selectedItemCount: Int
        get() = selectedItems.size()

    fun toggleSelection(pos: Int) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos)
        } else {
            selectedItems.put(pos, true)
        }

        notifyDataSetChanged()
    }


    override fun getCount(): Int {
        return artists.size
    }

    fun getSelectedItems(): List<Artist> {
        val items = ArrayList<Artist>(selectedItems.size())
        for (i in 0 until selectedItems.size()) {
            items.add(artists[selectedItems.keyAt(i)])
        }
        return items
    }

    fun selectAll() {
        for (i in 0 until count)
            selectedItems.put(i, true)
        notifyDataSetChanged()
    }

    fun deSelectAll() {
        selectedItems.clear()
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var converter = convertView
        val viewHolder: ViewHolder
        if (converter != null) {
            viewHolder = converter.tag as ViewHolder
        } else {
            val inflater = LayoutInflater.from(parent.context)
            converter = inflater.inflate(R.layout.item_track, parent, false)
            viewHolder = ViewHolder()
            viewHolder.name = converter.findViewById(R.id.title)
            viewHolder.description = converter.findViewById(R.id.description)
            viewHolder.image = converter.findViewById(R.id.image)
            viewHolder.checkBox = converter.findViewById(R.id.checkbox)
            converter.tag = viewHolder
        }

        if (!selectMulti) {
            viewHolder.checkBox.visibility = View.GONE
        } else {
            viewHolder.checkBox.visibility = View.VISIBLE
        }

        viewHolder.checkBox.isChecked = selectedItems.get(position)

        val artist = getItem(position)
        Glide.with(parent.context)
                .load(artist?.image)
                .placeholder(R.drawable.ic_track)
                .error(R.drawable.ic_track)
                .transform(RoundedCornersTransformation(
                        context.resources.getInteger(R.integer.rounding_radius_transform),
                        context.resources.getInteger(R.integer.margin_radius_transform)))
                .into(viewHolder.image)

        viewHolder.description.text = artist.numSongs.toString() + if (artist.numSongs > 1) "tracks" else "track"
        viewHolder.name.text = artist.name

        return converter!!
    }

    fun setSelectMulti(selectMulti: Boolean) {
        this.selectMulti = selectMulti
        notifyDataSetChanged()
    }

    class ViewHolder {
        lateinit var image: ImageView
        lateinit var name: TextView
        lateinit var description: TextView
        lateinit var checkBox: CheckBox
    }
}
