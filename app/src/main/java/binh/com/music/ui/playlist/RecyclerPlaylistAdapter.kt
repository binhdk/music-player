package binh.com.music.ui.playlist

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import binh.com.music.R
import binh.com.music.data.PlayList

/**
 * Created by binh on 2/26/2019.
 */

class RecyclerPlaylistAdapter(private var playLists: List<PlayList>) : RecyclerView.Adapter<RecyclerPlaylistAdapter.ViewHolder>() {
    private var listener: OnItemViewClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_playlist_typical, null)
        val viewHolder = ViewHolder(view)
        viewHolder.itemView.setOnClickListener {
            listener?.onClick(viewHolder.adapterPosition)

        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val playList = playLists[position]
        holder.title.text = if (!TextUtils.isEmpty(playList.name)) playList.name else ""
        holder.numTracks.text = if (playList.numTracks > 1)
            playList.numTracks.toString() + " tracks"
        else
            playList.numTracks.toString() + " tracks"

    }

    override fun getItemCount(): Int {
        return playLists.size
    }

    fun setListener(listener: OnItemViewClickListener) {
        this.listener = listener
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.title)
        var numTracks: TextView = itemView.findViewById(R.id.numTracks)

    }

    interface OnItemViewClickListener {
        fun onClick(position: Int)
    }
}
