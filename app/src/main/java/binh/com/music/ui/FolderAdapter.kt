package binh.com.music.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

import binh.com.music.R
import binh.com.music.data.Folder

/**
 * Created by binh on 2/21/2019.
 */

class FolderAdapter(context: Context,
                    resource: Int,
                    private var folders: List<Folder>
) : ArrayAdapter<Folder>(context, resource, folders) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var newConvertView = convertView
        val viewHolder: ViewHolder
        if (newConvertView != null) {
            viewHolder = newConvertView.tag as ViewHolder
        } else {
            val inflater = LayoutInflater.from(parent.context)
            newConvertView = inflater.inflate(R.layout.item_track, parent, false)
            viewHolder = ViewHolder()
            viewHolder.title = newConvertView.findViewById(R.id.title)
            viewHolder.description = newConvertView.findViewById(R.id.description)
            viewHolder.thumbnail = newConvertView.findViewById(R.id.image)
            newConvertView.tag = viewHolder
        }

        return newConvertView!!
    }

    class ViewHolder() {
        lateinit var description:TextView
        lateinit var title:TextView
        lateinit var thumbnail:ImageView
    }
}
