package binh.com.music.ui.track

import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import binh.com.music.App
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.Track
import binh.com.music.data.db.AppDatabase
import binh.com.music.ui.playlist.PlaylistChoiceActivity
import binh.com.music.util.IntentUtil
import binh.com.music.util.PersistentDataService
import binh.com.music.util.Util
import com.bumptech.glide.Glide
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import java.io.File
import java.io.Serializable
import java.util.*

/**
 * Created by binh on 3/16/2019.
 */

class TrackBottomSheetFragment : BottomSheetDialogFragment() {
    private lateinit var track: Track
    private var dialog: ProgressDialog? = null
    private var remove = false

    private val onClickListener = View.OnClickListener { v ->
        when (v.id) {
            R.id.add_to_favorites -> {
                addToFavorites()
                dismiss()
            }
            R.id.add_to_playlist -> {
                addToPlaylist()
                dismiss()
            }
            R.id.share -> {
                share()
                dismiss()
            }
            R.id.set_default_ringtone -> setAsRingtone()
            R.id.delete -> confirmDelete()
            R.id.remove -> {
                removeFromPlaylist()
                dismiss()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data = arguments.getSerializable(IntentUtil.EXTRA_TRACK) as? Track
        if (data == null) {
            dismiss()
        } else {
            track = data
        }

        if (arguments.containsKey("remove")) {
            remove = arguments.getBoolean("remove", false)
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.bottom_sheet_track, container, false)

        val addToFavorites = rootView.findViewById<TextView>(R.id.add_to_favorites)
        val addToPlaylist = rootView.findViewById<TextView>(R.id.add_to_playlist)
        val share = rootView.findViewById<TextView>(R.id.share)
        val delete = rootView.findViewById<TextView>(R.id.delete)
        val setRingtone = rootView.findViewById<TextView>(R.id.set_default_ringtone)
        val title = rootView.findViewById<TextView>(R.id.title)
        val artist = rootView.findViewById<TextView>(R.id.artist)
        val imageView = rootView.findViewById<ImageView>(R.id.image)
        val remove = rootView.findViewById<TextView>(R.id.remove)

        addToFavorites.setOnClickListener(onClickListener)
        addToPlaylist.setOnClickListener(onClickListener)
        share.setOnClickListener(onClickListener)
        delete.setOnClickListener(onClickListener)
        remove.setOnClickListener(onClickListener)
        setRingtone.setOnClickListener(onClickListener)

        if (this@TrackBottomSheetFragment.remove) {
            remove.visibility = View.VISIBLE
            delete.visibility = View.GONE
        } else {
            remove.visibility = View.GONE
            delete.visibility = View.VISIBLE
        }

        title.text = track.title
        artist.text = track.artist
        Glide.with(context)
                .load(track.image)
                .placeholder(R.drawable.ic_track)
                .error(R.drawable.ic_track)
                .transform(RoundedCornersTransformation(
                        context.resources.getInteger(R.integer.rounding_radius_transform),
                        context.resources.getInteger(R.integer.margin_radius_transform)))
                .into(imageView)
        return rootView
    }

    private fun removeFromPlaylist() {
        val intent = Intent(IntentUtil.ACTION_REMOVE_PLAYLIST_TRACK)
        intent.putExtra(IntentUtil.EXTRA_TRACK_ID, track.id)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    private fun setAsRingtone() {
        dialog = ProgressDialog(context)
        dialog?.setMessage("Processing...")
        dialog?.setCanceledOnTouchOutside(false)
        try {
            val contentValues = ContentValues()
            contentValues.put(MediaStore.Audio.Media._ID, track.id)
            contentValues.put(MediaStore.Audio.Media.IS_RINGTONE, true)
            context.contentResolver
                    .update(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                            contentValues,
                            MediaStore.Audio.Media._ID + "=" + track.id, null
                    )
            var contentUri = MediaStore.Audio.Media.getContentUriForPath(track.path)
            contentUri = Uri.withAppendedPath(contentUri, track.id.toString())

            if (Util.checkSelfForWriteSettingPermission(context)) {
                dialog?.show()
                val file = File(track.path)
                val folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES)
                if (!folder.exists()) {
                    folder.mkdir()
                }

                val newFile = File(folder.path + "/" + file.name)
                if (Util.copyFile(file, newFile)) {
                    RingtoneManager.setActualDefaultRingtoneUri(context,
                            RingtoneManager.TYPE_RINGTONE,
                            contentUri)

                    Toast.makeText(context, "Set default ringtone successfully", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(context, "Something went wrong, try later", Toast.LENGTH_SHORT).show()
                }
                dialog!!.dismiss()
            } else {
                val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS,
                        Uri.parse("package:" + context.packageName))
                startActivityForResult(intent, 200)
            }
        } catch (e: Exception) {
            Toast.makeText(context, "Something went wrong, try later", Toast.LENGTH_SHORT).show()
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }

        dialog!!.dismiss()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (dialog?.isShowing == true)
            dialog?.dismiss()
    }

    private fun share() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "audio/*"
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + track.path))
        startActivity(Intent.createChooser(intent, "Share audio file"))
    }

    private fun confirmDelete() {
        AlertDialog.Builder(context)
                .setMessage("Delete track?")
                .setPositiveButton(resources.getString(R.string.delete)) { dialog, which ->
                    deleteMedia()
                    dialog.dismiss()
                }
                .setNegativeButton(resources.getString(R.string.cancel)) { dialog, which -> dialog.dismiss() }
                .create()
                .show()
    }

    private fun deleteMedia() {
        try {
            Util.deleteTrackFromDevice(context, track)
            val intent = Intent(IntentUtil.ACTION_DELETE_TRACK)
            intent.putExtra(IntentUtil.EXTRA_TRACK_ID, track.id)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
            Toast.makeText(context, "Delete successfully", Toast.LENGTH_SHORT).show()

            //delete metadata of this track
            val intent2 = Intent(context, PersistentDataService::class.java)
            intent2.action = IntentUtil.ACTION_DELETE_TRACK
            intent2.putExtra(IntentUtil.EXTRA_TRACK_ID, track.id)
            context.startService(intent2)
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
            Toast.makeText(context, "Cannot delete", Toast.LENGTH_SHORT).show()
        }

        dismiss()
    }

    private fun addToPlaylist() {
        val tracks = ArrayList<Track>()
        tracks.add(track)
        val intent = Intent(activity, PlaylistChoiceActivity::class.java)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable)
        startActivity(intent)
    }

    private fun addToFavorites() {
        Thread(Runnable {
            try {
                val tmp = AppDatabase.getInstance(App.context).trackDao().getById(track.id)
                tmp.isFavorite = true
                val row = AppDatabase.getInstance(App.context).trackDao().update(tmp)

                if (row > 0) {
                    track.isFavorite = true
                    val intent = Intent(IntentUtil.ACTION_ADD_TO_FAVORITE)
                    intent.putExtra(IntentUtil.EXTRA_TRACK_ID, track.id)
                    LocalBroadcastManager
                            .getInstance(App.context)
                            .sendBroadcast(intent)
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()
    }

    companion object {

        @JvmStatic
        fun newInstance(track: Track) = TrackBottomSheetFragment().also {
            val args = Bundle()
            args.putSerializable(IntentUtil.EXTRA_TRACK, track)
            it.arguments = args
        }

        @JvmStatic
        fun newInstance(track: Track, remove: Boolean) = TrackBottomSheetFragment().also {
            val args = Bundle()
            args.putSerializable(IntentUtil.EXTRA_TRACK, track)
            args.putBoolean("remove", remove)
            it.arguments = args
        }
    }
}
