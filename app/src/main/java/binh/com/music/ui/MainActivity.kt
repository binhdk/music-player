package binh.com.music.ui

import android.content.*
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.SeekBar
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.ui.album.AlbumFragment
import binh.com.music.ui.artist.ArtistFragment
import binh.com.music.ui.playlist.PlayListFragment
import binh.com.music.ui.track.TrackFragment
import binh.com.music.util.Constant
import binh.com.music.util.IntentUtil
import binh.com.music.util.MediaPlayerService
import binh.com.music.util.Util
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.title as trackTitle


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private val seekBarHandler = Handler()
    private val seekBarRunnable = object : Runnable {
        override fun run() {
            if (isServiceBound && service!!.isPrepared) {
                seek_bar.max = (service!!.duration / 1000).toInt()
                seek_bar.progress = service!!.currentPosition / 1000
                seekBarHandler.postDelayed(this, 1000)
            }
        }
    }

    private var playbackReceiver: BroadcastReceiver? = null
    private var service: MediaPlayerService? = null
    private var bound = false
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MediaPlayerService.LocalBinder
            this@MainActivity.service = binder.service
            bound = true
            updateUI(this@MainActivity.service?.playbackStateCompat,
                    this@MainActivity.service?.metadata)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            bound = false
            updateUI(this@MainActivity.service?.playbackStateCompat,
                    this@MainActivity.service?.metadata)
        }
    }

    val isServiceBound: Boolean
        get() = bound && service != null

    private val seekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            seekBar.progress = progress
            if (isServiceBound && fromUser) {
                if (service!!.isPrepared) {
                    service!!.seekTo(progress * 1000)
                }
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        seek_bar.let {
            it.progressDrawable.colorFilter = PorterDuffColorFilter(
                    ContextCompat.getColor(this, android.R.color.white),
                    PorterDuff.Mode.MULTIPLY)
            it.thumb.setColorFilter(ContextCompat.getColor(this, R.color.seekbarColor), PorterDuff.Mode.SRC_IN)
            it.setOnSeekBarChangeListener(seekBarChangeListener)
        }


        prev.setOnClickListener(this)
        next.setOnClickListener(this)
        play.setOnClickListener(this)
        pause.setOnClickListener(this)

        media_layout.setOnClickListener(this)


        setUpViewPager(viewPager)

        registerReceivers()

        val intent = Intent()
        intent.setClass(this, MediaPlayerService::class.java)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)

        startService(Intent(this, MediaPlayerService::class.java))
    }

    fun updateSeekBar() {
        if (isServiceBound && service!!.isPrepared) {
            seekBarHandler.post(seekBarRunnable)
        } else {
            resetSeekBar()
        }
    }

    fun updateUI(playbackStateCompat: PlaybackStateCompat?, metadata: MediaMetadataCompat?) {
        updateSeekBar()
        if (metadata != null) {
            Glide.with(this)
                    .load(metadata.description.iconUri)
                    .error(R.drawable.ic_track)
                    .placeholder(R.drawable.ic_track)
                    .circleCrop()
                    .into(image)
            trackTitle.text = metadata.description.title
            artist.text = metadata.description.subtitle
        } else {
            image.setImageResource(R.drawable.ic_track)
            trackTitle.text = "Empty queue"
            artist.text = ""
            seekBarHandler.removeCallbacks(seekBarRunnable)
            seek_bar.progress = 0
        }

        if (playbackStateCompat != null && playbackStateCompat.state == PlaybackStateCompat.STATE_PLAYING) {
            pause.visibility = View.VISIBLE
            play.visibility = View.GONE
        } else {

            pause.visibility = View.GONE
            play.visibility = View.VISIBLE
        }
    }


    override fun onResume() {
        super.onResume()
        if (!Util.checkSelfForStoragePermission(this))
            Util.requestPermissions(this, Constant.PERMISSIONS_STORAGE, Constant.REQUEST_STORAGE)
    }

    override fun onDestroy() {
        super.onDestroy()
        // Unbind from the service
        if (bound) {
            unbindService(connection)
            bound = false
        }
        unregisterReceivers()
        seekBarHandler.removeCallbacks(seekBarRunnable)
        if (BuildConfig.DEBUG) {
            Log.d(MainActivity::class.java.simpleName, "onDestroy")
        }
    }

    fun registerReceivers() {
        playbackReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action
                if (!TextUtils.isEmpty(action)) {
                    val state = intent.getParcelableExtra<PlaybackStateCompat>("playbackstate")
                    val metadata = intent.getParcelableExtra<MediaMetadataCompat>("metadata")
                    updateUI(state, metadata)
                }
            }
        }

        val filter1 = IntentFilter()
        filter1.addAction(IntentUtil.PLAYBACK_STATE_CHANGE)

        LocalBroadcastManager.getInstance(this).registerReceiver(playbackReceiver, filter1)
    }

    private fun resetSeekBar() {
        seek_bar.progress = 0
        seekBarHandler.removeCallbacks(seekBarRunnable)
    }

    fun unregisterReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(playbackReceiver)
    }


    private fun setUpViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(PlayListFragment.newInstance(), resources.getString(R.string.playlist))
        adapter.addFragment(AlbumFragment.newInstance(), resources.getString(R.string.album))
        adapter.addFragment(TrackFragment.newInstance(), resources.getString(R.string.track))
        adapter.addFragment(ArtistFragment.newInstance(), resources.getString(R.string.artist))
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 4
        tabLayout.setupWithViewPager(viewPager)
    }

    //    @Override
    //    public boolean onCreateOptionsMenu(Menu menu) {
    //        getMenuInflater().inflate(R.menu.main, menu);
    //        return true;
    //    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constant.REQUEST_STORAGE) {
            if (grantResults.size == 2) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Util.shouldShowRequestForStoragePermission(this)
                } else {
                    LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(IntentUtil.PERMISSION_STORAGE_GRANTED))
                }
            } else {
                Util.shouldShowRequestForStoragePermission(this)
            }
        }
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.play -> play()
            R.id.pause -> pause()
            R.id.next -> {
                resetSeekBar()
                next()
            }
            R.id.prev -> {
                resetSeekBar()
                prev()
            }
            R.id.media_layout -> {
                startActivity(Intent(this@MainActivity, MediaPlayerActivity::class.java))
                overridePendingTransition(R.anim.effect_pull_in_top, R.anim.effect_do_nothing)
            }
        }
    }


    private fun prev() {
        if (isServiceBound)
            service?.previous()
    }

    private operator fun next() {
        if (isServiceBound)
            service?.next()
    }

    private fun pause() {
        if (isServiceBound)
             service?.pause()
    }

    private fun play() {
        if (isServiceBound)
            service?.play()

    }
}
