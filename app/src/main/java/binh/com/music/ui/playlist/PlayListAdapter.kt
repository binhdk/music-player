package binh.com.music.ui.playlist

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import binh.com.music.R
import binh.com.music.data.PlayList

/**
 * Created by binh on 2/21/2019.
 *
 */

class PlayListAdapter(context: Context,
                      resource: Int,
                      private var playLists: List<PlayList>) : ArrayAdapter<PlayList>(context, resource, playLists) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var converter = convertView
        val viewHolder: ViewHolder
        if (converter != null) {
            viewHolder = converter.tag as ViewHolder
        } else {
            val inflater = LayoutInflater.from(parent.context)
            converter = inflater.inflate(R.layout.item_playlist, parent, false)
            viewHolder = ViewHolder()
            viewHolder.title = converter.findViewById(R.id.title)
            viewHolder.numTracks = converter.findViewById(R.id.numTracks)
            converter.tag = viewHolder
        }

        val playList = getItem(position)
        viewHolder.title.text = if (!TextUtils.isEmpty(playList!!.name)) playList.name else ""
        viewHolder.numTracks.text = if (playList.numTracks > 1)
            playList.numTracks.toString() + " tracks"
        else
            playList.numTracks.toString() + " track"

        return converter!!
    }

    class ViewHolder {
        lateinit var title: TextView
        lateinit var numTracks: TextView
    }
}
