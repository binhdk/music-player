package binh.com.music.ui.track

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.AdapterView
import binh.com.music.R
import binh.com.music.data.Track
import binh.com.music.util.IntentUtil
import binh.com.music.util.Util
import kotlinx.android.synthetic.main.activity_track_selection.*
import java.io.Serializable

class TrackSelectionActivity : AppCompatActivity() {
    private lateinit var tracks: List<Track>
    private lateinit var adapter: TrackAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_selection)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        all.setOnClickListener{
            toggleAll()
        }
        done.setOnClickListener {
            val intent = Intent()
            intent.putExtra(IntentUtil.EXTRA_TRACKS, adapter.getSelectedItems() as Serializable)
            setResult(Activity.RESULT_OK, intent)
            onBackPressed()
        }

        tracks = Util.getOriginTracks(this)
        adapter = TrackAdapter(this, R.layout.item_track, tracks)
        adapter.isSelectMulti = true

        list.adapter = adapter

        list.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id -> toggleItem(position) }
    }

    private fun toggleItem(position: Int) {
        adapter.toggleSelection(position)
        val selectedItemCount = adapter.selectedItemCount
        if (selectedItemCount == 0) {
            count.text = "SELECT TRACKS"
            done.visibility = View.GONE
        } else {
            count.text = selectedItemCount.toString()
            done.visibility = View.VISIBLE
        }
    }

    private fun toggleAll() {
        all.toggle()
        if (all.isChecked) {
            adapter.selectAll()
            count.text = adapter.selectedItemCount.toString()
            done.visibility = View.VISIBLE
        } else {
            adapter.deSelectAll()
            count.text = "SELECT TRACKS"
            done.visibility = View.GONE
        }
    }


}
