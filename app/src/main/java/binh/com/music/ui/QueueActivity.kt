package binh.com.music.ui

import android.app.Activity
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.Track
import binh.com.music.ui.track.TrackSelectionActivity
import binh.com.music.util.*
import com.woxthebox.draglistview.DragListView
import kotlinx.android.synthetic.main.activity_queue.*
import java.util.*

class QueueActivity : AppCompatActivity() {
    private val REQUEST_CODE_GET_TRACK = 2
    private lateinit var adapter: QueueAdapter
    private var service: MediaPlayerService? = null
    private var bound = false
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MediaPlayerService.LocalBinder
            this@QueueActivity.service = binder.service
            bound = true

            updateQueue()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            bound = false
        }
    }
    private var playbackReceiver: BroadcastReceiver? = null

    private val isServiceBound: Boolean
        get() = bound && service != null

    private var tracks: MutableList<Track> = ArrayList()

    private val lastPlay: Int
        get() {
            for (i in 0 until tracks.size - 1) {
                if (tracks[i].isLastPlay)
                    return i
            }

            return 0
        }

    private fun updateQueue() {
        if (isServiceBound) {
            tracks.clear()
            tracks.addAll(service!!.getQueue())
            adapter.notifyDataSetChanged()
            adapter.setCurrentPosition(service!!.currentIndex)
            updateCurrent()
        }
    }

    private fun updateCurrent() {
        if (isServiceBound) {
            count!!.text = (service!!.currentIndex + 1).toString() + "/" + tracks.size
            adapter.setCurrentPosition(service!!.currentIndex)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_queue)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setTitle(R.string.queue)

        drag_list_view.setDragListListener(object : DragListView.DragListListener {
            override fun onItemDragStarted(position: Int) {

            }

            override fun onItemDragging(itemPosition: Int, x: Float, y: Float) {

            }

            override fun onItemDragEnded(fromPosition: Int, toPosition: Int) {
                if (isServiceBound) {
                    if (fromPosition == service!!.currentIndex) {
                        service!!.setQueue(tracks, toPosition)
                    } else if (fromPosition < toPosition && fromPosition <= service!!.currentIndex && service!!.currentIndex <= toPosition) {
                        service!!.setQueue(tracks, service!!.currentIndex - 1)
                    } else if (fromPosition > toPosition && toPosition <= service!!.currentIndex && service!!.currentIndex <= fromPosition) {
                        service!!.setQueue(tracks, service!!.currentIndex + 1)
                    } else {
                        service!!.setQueue(tracks, service!!.currentIndex)
                    }

                    updateCurrent()
                }
            }
        })

        adapter = QueueAdapter(tracks, R.layout.item_track, R.id.move, false,this)
        drag_list_view.setLayoutManager(LinearLayoutManager(this))
        drag_list_view.setAdapter(adapter, false)
        drag_list_view.setCanDragHorizontally(false)



      adapter.setItemClickListener(object: QueueAdapter.OnItemClickListener {
            override fun onClick(v: View, position: Int) {
                try {
                    if (isServiceBound) {
                        service!!.setCurrent(position)
                        service!!.pause()
                        service!!.play()
                        adapter.setCurrentPosition(service!!.currentIndex)
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: IndexOutOfBoundsException) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onLongClick(v: View, position: Int) {
                try {
                    showItemOptions(v, position)
                } catch (e: IndexOutOfBoundsException) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }

            }
        })


        sort!!.setOnClickListener { v -> showSortOptions(v) }

        registerReceivers()

        val intent = Intent()
        intent.setClass(this, MediaPlayerService::class.java)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }

    private fun showItemOptions(v: View, position: Int) {
        val popupMenu = PopupMenu(this, v)
        popupMenu.inflate(R.menu.menu_context_queue_item)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.remove -> removeFromQueue(position)
            }
            true
        }
        popupMenu.show()
    }

    private fun removeFromQueue(position: Int) {
        if (isServiceBound) {
            service!!.removeQueueItem(position)
            tracks.removeAt(position)
            adapter.setCurrentPosition(service!!.currentIndex)
            adapter.notifyDataSetChanged()
            updateCurrent()
        }
    }

    private fun showSortOptions(v: View) {
        val popupMenu = PopupMenu(this, v)
        popupMenu.inflate(R.menu.menu_sort_playlist_track)
        val sortName = popupMenu.menu.findItem(R.id.sort_name)
        val sortArtist = popupMenu.menu.findItem(R.id.sort_artist)

        val sort = PreferenceManager.getDefaultSharedPreferences(this)
                .getInt("queue-sort", Sort.NAME)
        if (sort == Sort.ARTIST) {
            val s = SpannableString(sortArtist.title)
            s.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 0, s.length, 0)
            sortArtist.title = s
        } else {
            val s = SpannableString(sortName.title)
            s.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 0, s.length, 0)
            sortName.title = s
        }
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.sort_name -> sortByName()
                R.id.sort_artist -> sortByArtist()
            }
            true
        }
        popupMenu.show()
    }

    private fun setLastPlay() {
        if (tracks.isEmpty())
            return
        if (isServiceBound) {
            for (track in tracks) {
                track.isLastPlay = false
            }
            tracks[service!!.currentIndex].isLastPlay = true
        }
    }

    private fun sortByArtist() {
        if (isServiceBound) {
            setLastPlay()
            tracks.sortWith(Comparator { o1, o2 ->
                val collator = Util.getCollator("vi_VN")
                collator.compare(o1.artist.toLowerCase(), o2.artist.toLowerCase())
            })

            adapter.notifyDataSetChanged()
            PreferenceManager.getDefaultSharedPreferences(this)
                    .edit()
                    .putInt("queue-sort", Sort.ARTIST)
                    .apply()

            service!!.setQueue(tracks, lastPlay)
            updateCurrent()
        }

    }

    private fun sortByName() {
        if (isServiceBound) {
            setLastPlay()

            tracks.sortWith(Comparator { o1, o2 ->
                val collator = Util.getCollator("vi_VN")
                collator.compare(o1.title.toLowerCase(), o2.title.toLowerCase())
            })

            adapter.notifyDataSetChanged()
            PreferenceManager.getDefaultSharedPreferences(this)
                    .edit()
                    .putInt("queue-sort", Sort.NAME)
                    .apply()
            service!!.setQueue(tracks, lastPlay)
            updateCurrent()
        }
    }

    fun registerReceivers() {
        playbackReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.PLAYBACK_STATE_CHANGE -> updateCurrent()
                    }
                }
            }
        }

        val filter1 = IntentFilter()
        filter1.addAction(IntentUtil.PLAYBACK_STATE_CHANGE)

        LocalBroadcastManager.getInstance(this).registerReceiver(playbackReceiver, filter1)
    }

    fun unregisterReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(playbackReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (bound) {
            unbindService(connection)
            bound = false
        }

        unregisterReceivers()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_queue_page, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_track -> startActivityForResult(
                    Intent(this@QueueActivity, TrackSelectionActivity::class.java),
                    REQUEST_CODE_GET_TRACK)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_GET_TRACK) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    val selectedTracks = data.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                    onTracksSelected(selectedTracks)

                } catch (e: ClassCastException) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }

            }
        }
    }

    private fun onTracksSelected(selectedTracks: List<Track>) {
        if (isServiceBound) {
            tracks.addAll(selectedTracks)
            adapter.notifyDataSetChanged()
            updateCurrent()
            service!!.addQueueItems(selectedTracks)
        }

    }


}
