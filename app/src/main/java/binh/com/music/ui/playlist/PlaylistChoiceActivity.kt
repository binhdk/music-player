package binh.com.music.ui.playlist

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.database.SQLException
import android.os.Bundle
import android.support.annotation.IntDef
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import binh.com.music.App
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.PlayList
import binh.com.music.data.Track
import binh.com.music.data.db.AppDatabase
import binh.com.music.util.Constant
import binh.com.music.util.IntentUtil
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_playlist_selection.*
import java.io.Serializable
import java.util.*

class PlaylistChoiceActivity : AppCompatActivity() {
    private var choices: MutableList<PlaylistChoice> = ArrayList()
    private lateinit var adapter: PlaylistChoiceAdapter
    private lateinit var tracks: List<Track>
    private var dialog: AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_selection)

        val tmp: List<Track>? = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as? List<Track>
        if (tmp.isNullOrEmpty())
            onBackPressed()
        else
            tracks = tmp

        adapter = PlaylistChoiceAdapter(this, R.layout.item_playlist, choices)
        list.adapter = adapter
        list.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val choice = parent.getItemAtPosition(position) as PlaylistChoice
            this@PlaylistChoiceActivity.onItemClick(choice)
        }

        loadPlaylist()
    }

    private fun loadPlaylist() {
        Thread(Runnable {
            val playListIconPlaceholder = Constant.DRAWABLE_RES_URI + "ic_track"
            choices.add(PlaylistChoice(0, PlaylistChoice.NEW_PLAYLIST, Constant.DRAWABLE_RES_URI + "ic_add_playlist_round", getString(R.string.create_playlist)))
            choices.add(PlaylistChoice(0, PlaylistChoice.QUEUE, Constant.DRAWABLE_RES_URI + "ic_add_to_queue_round", getString(R.string.queue)))
            choices.add(PlaylistChoice(0, PlaylistChoice.FAVORITE, Constant.DRAWABLE_RES_URI + "ic_favorite_round", getString(R.string.add_to_favorite)))

            val playLists = AppDatabase.getInstance(this@PlaylistChoiceActivity).playListDao().all
            for (playList in playLists) {
                choices.add(PlaylistChoice(playList.id,
                        PlaylistChoice.PLAYLIST,
                        playListIconPlaceholder,
                        playList.name))
            }

            runOnUiThread { adapter.notifyDataSetChanged() }
        }).start()
    }

    private fun onItemClick(choice: PlaylistChoice) {
        when (choice.type) {
            PlaylistChoice.QUEUE -> addTrackToQueue()
            PlaylistChoice.PLAYLIST -> addTracksToPlaylist(choice.id)
            PlaylistChoice.NEW_PLAYLIST -> showDialogAddPlaylist()
            PlaylistChoice.FAVORITE -> addToFavorite()
        }
    }

    private fun addToFavorite() {
        for (track in tracks) {
            track.isFavorite = true
        }

        val intent = Intent(IntentUtil.ACTION_ADD_TRACKS_TO_FAVORITE)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable?)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

        Thread(Runnable {
            try {
                AppDatabase.getInstance(App.context).trackDao().update(tracks)
            } catch (e: SQLException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()
        onBackPressed()
    }

    private fun addTracksToPlaylist(playlistId: Long) {
        Thread(Runnable {
            try {
                val playList: PlayList? = AppDatabase.getInstance(App.context).playListDao().getOne(playlistId)
                if (playList != null) {
                    playList.addTracks(tracks)
                    AppDatabase.getInstance(App.context).playListDao().update(playList)
                    val intent = Intent(IntentUtil.ACTION_UPDATE_PLAYLIST)
                    intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
                    LocalBroadcastManager.getInstance(App.context).sendBroadcast(intent)
                }
            } catch (e: SQLException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()

        onBackPressed()
    }

    private fun addTrackToQueue() {
        val intent = Intent(IntentUtil.ACTION_ADD_QUEUE_ITEMS)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable?)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        onBackPressed()
    }

    private fun showDialogAddPlaylist() {
        dialog = AlertDialog.Builder(this)
                .setTitle("Create playlist")
                .setPositiveButton("OK") { dialog, which ->
                    val playlistName = this@PlaylistChoiceActivity.dialog!!.findViewById<EditText>(R.id.playlist_name)
                    addPlaylist(playlistName.text.toString())
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }
                .create()
        val view = layoutInflater.inflate(R.layout.dialog_add_playlist, null)
        dialog?.setView(view)
        dialog?.show()
        val playlistName = dialog!!.findViewById<EditText>(R.id.playlist_name)
        playlistName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (TextUtils.isEmpty(s)) {
                    playlistName.error = "None empty name"
                } else {
                    playlistName.error = null
                }
            }
        })
    }

    private fun addPlaylist(text: String) {
        if (!TextUtils.isEmpty(text)) {
            val playList = PlayList(text)
            Thread(Runnable {
                try {
                    val id = AppDatabase.getInstance(App.context).playListDao().insert(playList)
                    if (id >= 0) {
                        playList.id = id
                        playList.addTracks(tracks)
                        AppDatabase.getInstance(this@PlaylistChoiceActivity).playListDao().update(playList)
                        var intent = Intent(this@PlaylistChoiceActivity, PlayListDetailActivity::class.java)
                        intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
                        startActivity(intent)

                        intent = Intent(IntentUtil.ACTION_ADD_PLAYLIST)
                        intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
                        LocalBroadcastManager.getInstance(App.context).sendBroadcast(intent)
                        finish()
                    }
                } catch (e: SQLException) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                    onBackPressed()
                }
            }).start()
        }
    }

    class PlaylistChoiceAdapter(context: Context,
                                resource: Int,
                                private var playlistChoices: List<PlaylistChoice>) : ArrayAdapter<PlaylistChoice>(context, resource, playlistChoices) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var conveter = convertView
            val viewHolder: ViewHolder
            if (conveter != null) {
                viewHolder = conveter.tag as ViewHolder
            } else {
                val inflater = LayoutInflater.from(parent.context)
                conveter = inflater.inflate(R.layout.item_playlist, parent, false)
                viewHolder = ViewHolder()
                viewHolder.title = conveter.findViewById(R.id.title)
                viewHolder.image = conveter.findViewById(R.id.image)
                conveter.tag = viewHolder
            }

            val choice = getItem(position)
            viewHolder.title?.text = if (!TextUtils.isEmpty(choice!!.title)) choice.title else ""
            Glide.with(parent.context)
                    .load(choice.icon)
                    .error(R.drawable.ic_track)
                    .placeholder(R.drawable.ic_track)
                    .into(viewHolder.image!!)


            return conveter!!
        }

        class ViewHolder {
            var title: TextView? = null
            var image: ImageView? = null

        }
    }

    class PlaylistChoice(var id: Long, @Type var type: Int, var icon: String, var title: String) : Serializable {

        @Target(AnnotationTarget.VALUE_PARAMETER)
        @IntDef(QUEUE.toLong(), FAVORITE.toLong(), NEW_PLAYLIST.toLong(), PLAYLIST.toLong())
        annotation class Type

        companion object {
            const val QUEUE = 0
            const val FAVORITE = 1
            const val NEW_PLAYLIST = 2
            const val PLAYLIST = 3
        }
    }
}
