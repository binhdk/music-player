package binh.com.music.ui.album

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import binh.com.music.R
import binh.com.music.data.Album
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

/**
 * Created by binh on 2/21/2019.
 */

class AlbumAdapter(context: Context,
                   resource: Int,
                   private var albums: List<Album>) : ArrayAdapter<Album>(context, resource, albums) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var newConverter = convertView
        val viewHolder: ViewHolder
        if (newConverter != null) {
            viewHolder = newConverter.tag as ViewHolder
        } else {
            val inflater = LayoutInflater.from(parent.context)
            newConverter = inflater.inflate(R.layout.item_album, parent, false)
            viewHolder = ViewHolder()
            viewHolder.title = newConverter!!.findViewById(R.id.title)
            viewHolder.description = newConverter.findViewById(R.id.description)
            viewHolder.image = newConverter.findViewById(R.id.image)
            newConverter.tag = viewHolder
        }

        val album = getItem(position)

        Glide.with(parent.context)
                .load(album.art)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(RoundedCornersTransformation(
                        context.resources.getInteger(R.integer.album_rounding_radius_transform), 0))
                .into(viewHolder.image)
        viewHolder.title.text = album.name
        viewHolder.description.text = album.artist + "(" + album.numSong + ")"

        return newConverter
    }

    class ViewHolder() {
        lateinit var description: TextView
        lateinit var title: TextView
        lateinit var image: ImageView
    }
}
