package binh.com.music.ui.playlist

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.database.SQLException
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.LocalBroadcastManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import binh.com.music.App
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.PlayList
import binh.com.music.data.db.AppDatabase
import binh.com.music.util.IntentUtil
import kotlinx.android.synthetic.main.bottom_sheet_playlist.*
import java.io.Serializable

/**
 * Created by binh on 3/19/2019.
 */

class PlayListBottomSheetFragment : BottomSheetDialogFragment() {
    private val REQUEST_CODE_SELECT_PLAYLIST = 222
    private var dialog: AlertDialog? = null
    private lateinit var playList: PlayList

    private val clickListener = View.OnClickListener { v ->
        when (v.id) {
            R.id.add_to_playlist -> addToPlaylist()
            R.id.play -> playPlaylist()
            R.id.delete -> confirmDeletePlaylist()
            R.id.rename -> showDialogRename()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val tmp = arguments.getSerializable(IntentUtil.EXTRA_PLAYLIST) as? PlayList
        if (tmp != null) {
            playList = PlayList(tmp.id, tmp.name, tmp.tracks)
        } else {
            dismiss()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_playlist, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        play.setOnClickListener(clickListener)
        add_to_playlist.setOnClickListener(clickListener)
        delete.setOnClickListener(clickListener)
        rename.setOnClickListener(clickListener)

    }

    private fun showDialogRename() {
        dialog = AlertDialog.Builder(activity)
                .setTitle("Rename playlist")
                .setPositiveButton("OK") { dialog, which ->
                    val playlistName = this@PlayListBottomSheetFragment.dialog!!.findViewById<EditText>(R.id.playlist_name)
                    rename(playlistName.text.toString())
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }
                .create()
        val view = layoutInflater.inflate(R.layout.dialog_add_playlist, null)
        dialog!!.setView(view)
        dialog!!.show()
        val playlistName = this@PlayListBottomSheetFragment.dialog!!.findViewById<EditText>(R.id.playlist_name)
        playlistName.setText(playList.name)
        playlistName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (TextUtils.isEmpty(s)) {
                    playlistName.error = "None empty name"
                } else {
                    playlistName.error = null
                }
            }
        })
    }

    private fun rename(name: String) {
        if (!TextUtils.isEmpty(name)) {
            playList.name = name
            val intent = Intent(IntentUtil.ACTION_UPDATE_PLAYLIST)
            intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
            LocalBroadcastManager.getInstance(App.context).sendBroadcast(intent)
        }
        dismiss()
    }

    private fun confirmDeletePlaylist() {
        dialog = AlertDialog.Builder(activity)
                .setMessage("Delete playlist?")
                .setPositiveButton("DELETE") { dialog, which ->
                    delete()
                    dialog.dismiss()
                    dismiss()
                }
                .setNegativeButton("CANCEL") { dialog, which -> dialog.dismiss() }
                .create()
        dialog!!.show()
    }

    private fun delete() {
        val intent = Intent(IntentUtil.ACTION_DELETE_PLAYLIST)
        intent.putExtra(IntentUtil.EXTRA_PLAYLIST_ID, playList.id)
        LocalBroadcastManager.getInstance(App.context).sendBroadcast(intent)

    }


    private fun playPlaylist() {
        if (playList.tracks.isEmpty()) {
            Toast.makeText(context, "Cannot play empty playlist", Toast.LENGTH_SHORT).show()
        } else {
            val intent = Intent(IntentUtil.ACTION_PLAY_PLAYLIST)
            intent.putExtra(IntentUtil.EXTRA_PLAYLIST, playList)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
        dismiss()
    }

    private fun addToPlaylist() {
        val intent = Intent(activity, PlaylistChoiceActivity::class.java)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, playList.tracks as Serializable)
        startActivityForResult(intent, REQUEST_CODE_SELECT_PLAYLIST)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SELECT_PLAYLIST) {
            if (resultCode == Activity.RESULT_OK) {
                val playList = data!!.getSerializableExtra(IntentUtil.EXTRA_PLAYLIST) as PlayList
                addToOtherPlayList(playList)
            }
        }
    }

    private fun addToOtherPlayList(playList: PlayList) {
        playList.tracks.addAll(this.playList.tracks)
        Thread(Runnable {
            try {
                AppDatabase.getInstance(App.context).playListDao().update(playList)
            } catch (e: SQLException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()

        dismiss()
    }

    companion object {

        @JvmStatic
        fun newInstance(playList: PlayList) = PlayListBottomSheetFragment().also {
            val args = Bundle()
            args.putSerializable(IntentUtil.EXTRA_PLAYLIST, playList)
            it.arguments = args
        }
    }
}
