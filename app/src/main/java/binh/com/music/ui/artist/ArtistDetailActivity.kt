package binh.com.music.ui.artist

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Toast
import binh.com.music.R
import binh.com.music.data.Artist
import binh.com.music.data.Track
import binh.com.music.ui.track.TrackAdapter
import binh.com.music.ui.track.TrackBottomSheetFragment
import binh.com.music.util.IntentUtil
import binh.com.music.util.Playback
import binh.com.music.util.Util
import kotlinx.android.synthetic.main.activity_artist_detail.*
import java.io.Serializable
import java.util.*


class ArtistDetailActivity : AppCompatActivity() {

    private lateinit var artist: Artist
    private var tracks: MutableList<Track> = ArrayList()
    private lateinit var adapter: TrackAdapter
    private var mediaChangeReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_detail)

        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)

        val data = intent.getSerializableExtra(IntentUtil.EXTRA_ARTIST) as? Artist

        if (data == null) {
            onBackPressed()
        } else {
            artist = data
        }

        title = artist.name

        adapter = TrackAdapter(this, R.layout.item_track, tracks)

        adapter.setListener(object : TrackAdapter.OnActionButtonClickListener {
            override fun onButtonAddToQueueClick(position: Int) {
                addTrackToQueue(tracks[position])
            }

            override fun onButtonMoreClick(position: Int) {
                showTrackMoreAction(tracks[position])
            }
        })
        list.adapter = adapter
        list.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id -> playArtistTrack(position) }

        val play = findViewById<ImageView>(R.id.play)
        play.setOnClickListener { playArtistTrack(0) }

        val shuffle = findViewById<ImageView>(R.id.shuffle)
        shuffle.setOnClickListener {
            val intent = Intent(Playback.ACTION_SHUFFLE)
            intent.putExtra(Playback.EXTRA_SHUFFLE, true)
            LocalBroadcastManager.getInstance(this@ArtistDetailActivity).sendBroadcast(intent)

            playArtistTrack(0)
        }

        registerReceiver()
        getAlbumTrack()

    }

    private fun getAlbumTrack() {
        tracks.clear()
        tracks.addAll(Util.getTracks(this,
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Audio.Media.ARTIST_ID + " = " + artist.id,
                null,
                MediaStore.Audio.Media.TITLE + " ASC"))
        adapter.notifyDataSetChanged()
    }

    private fun showTrackMoreAction(track: Track) {
        val fragment = TrackBottomSheetFragment.newInstance(track)
        fragment.show(supportFragmentManager, fragment.tag)

    }

    private fun addTrackToQueue(track: Track) {
        Toast.makeText(this, getString(R.string.add_to_queue_successfull), Toast.LENGTH_SHORT).show()
        val intent = Intent(IntentUtil.ACTION_ADD_QUEUE_ITEM)
        intent.putExtra(IntentUtil.EXTRA_TRACK, track)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun playArtistTrack(lastPosition: Int) {
        tracks[lastPosition].isLastPlay = true
        val intent = Intent(IntentUtil.ACTION_PLAY_ARTIST_TRACKS)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_album_detail, menu)

        val addToFavorite = menu.findItem(R.id.add_to_favorites)
        val removeFromFavorite = menu.findItem(R.id.remove_from_favorites)
        val favorite = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean("artist-" + artist.id, false)
        if (favorite) {
            removeFromFavorite.isVisible = true
            addToFavorite.isVisible = false
        } else {
            removeFromFavorite.isVisible = false
            addToFavorite.isVisible = true
        }
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_to_favorites -> changeFavorites(true)
            R.id.remove_from_favorites -> changeFavorites(false)
        }

        return super.onOptionsItemSelected(item)
    }


    private fun changeFavorites(favorite: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putBoolean("artist-" + artist.id, favorite)
                .apply()
        invalidateOptionsMenu()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun registerReceiver() {

        mediaChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.ACTION_ADD_TO_FAVORITE -> {
                            val id = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            val favorite = intent.getBooleanExtra(IntentUtil.EXTRA_FAVORITE, false)
                            if (id > 0)
                                addMediaToFavorite(id,favorite)
                        }
                        IntentUtil.ACTION_DELETE_TRACK -> {
                            val id = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            if (id > 0) {
                                deleteTrack(id)
                            }
                        }

                        IntentUtil.ACTION_DELETE_TRACKS -> {
                            val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                            deleteTrack(tracks)
                        }
                    }
                }
            }
        }

        val filter2 = IntentFilter()
        filter2.addAction(IntentUtil.ACTION_ADD_TO_FAVORITE)
        filter2.addAction(IntentUtil.ACTION_DELETE_TRACK)
        filter2.addAction(IntentUtil.ACTION_DELETE_TRACKS)

        LocalBroadcastManager.getInstance(this).registerReceiver(mediaChangeReceiver, filter2)

    }

    private fun deleteTrack(tracks: List<Track>) {
        val deletes = ArrayList<Track>()
        for (track in tracks) {
            for (q in this.tracks) {
                if (track.id == q.id) {
                    deletes.add(q)
                }
            }
        }

        this.tracks.removeAll(deletes)
        adapter.notifyDataSetChanged()

    }

    fun unregisterReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mediaChangeReceiver)
    }


    private fun deleteTrack(id: Long) {
        val removes = ArrayList<Track>()
        removes.addAll(tracks.filter { it.id == id })


        tracks.removeAll(removes)
        adapter.notifyDataSetChanged()
    }


    private fun addMediaToFavorite(id: Long,favorite: Boolean) {
        for (track in tracks) {
            if (id == track.id) {
                track.isFavorite = favorite
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver()
    }
}
