package binh.com.music.ui.artist

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.Cursor
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.view.*
import android.widget.AdapterView
import binh.com.music.R
import binh.com.music.data.Artist
import binh.com.music.data.Track
import binh.com.music.ui.playlist.PlaylistChoiceActivity
import binh.com.music.util.IntentUtil
import binh.com.music.util.Util
import kotlinx.android.synthetic.main.fragment_track.*
import java.io.Serializable
import java.util.*


/**
 * Created by binh on 2/23/2019.
 * display artists
 */

class ArtistFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {
    private val ARTIST_LOADER_ID = 2
    private var artists: ArrayList<Artist> = ArrayList()
    private lateinit var adapter: ArtistAdapter

    private var storagePermissionGrantedReceiver: BroadcastReceiver? = null

    private var actionMode: ActionMode? = null
    private var isSelectedAll = false

    private val actionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            mode.menuInflater.inflate(R.menu.menu_artist_action_mode, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.delete -> {
                    confirmDelete(adapter.getSelectedItems())
                    dismissActionMode()
                }
                R.id.play -> {
                    playArtistTrack(adapter.getSelectedItems())
                    dismissActionMode()
                }
                R.id.add_to_playlist -> {
                    addToPlaylist(adapter.getSelectedItems())
                    dismissActionMode()
                }
                R.id.select_deselect_all -> selectDeselectAll()
            }
            return true
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            listView.clearChoices()
            adapter.deSelectAll()
            adapter.setSelectMulti(false)
            actionMode = null
            isSelectedAll = false
        }
    }

    fun registerReceiver() {
        storagePermissionGrantedReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                loaderManager.initLoader(ARTIST_LOADER_ID, null, this@ArtistFragment)
            }
        }
        val filter1 = IntentFilter(IntentUtil.PERMISSION_STORAGE_GRANTED)

        LocalBroadcastManager.getInstance(context).registerReceiver(storagePermissionGrantedReceiver, filter1)

    }

    private fun addToPlaylist(selectedItems: List<Artist>) {
        val tracks = ArrayList<Track>()

        for (artist in selectedItems) {
            tracks.addAll(Util.getTracks(context,
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    MediaStore.Audio.Media.ARTIST_ID + " = " + artist.id,
                    null,
                    MediaStore.Audio.Media.TITLE + " asc"))

        }

        val intent = Intent(activity, PlaylistChoiceActivity::class.java)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable)
        startActivity(intent)

    }

    private fun confirmDelete(selectedItems: List<Artist>) {
        AlertDialog.Builder(context)
                .setMessage("Do you want confirmDelete these items?")
                .setNegativeButton(getString(R.string.cancel)) { dialog, which -> dialog.dismiss() }
                .setPositiveButton(getString(R.string.delete)) { dialog, which ->
                    dialog.dismiss()
                    dismissActionMode()
                    delete(selectedItems)
                }
                .create()
                .show()

    }

    private fun delete(selectedItems: List<Artist>) {
        val tracks = ArrayList<Track>()

        for (artist in selectedItems) {
            tracks.addAll(Util.getTracks(context,
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    MediaStore.Audio.Media.ARTIST_ID + " = " + artist.id,
                    null,
                    MediaStore.Audio.Media.TITLE + " asc"))

        }

        Util.deleteTracksFromDevice(context, tracks)

        val intent = Intent(IntentUtil.ACTION_DELETE_TRACKS)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

        artists.removeAll(selectedItems)
        adapter.notifyDataSetChanged()
    }

    private fun selectDeselectAll() {
        if (adapter.selectedItemCount == 0) {
            isSelectedAll = true
            adapter.selectAll()
        } else if (adapter.selectedItemCount == adapter.count) {
            adapter.deSelectAll()
        } else if (isSelectedAll) {
            isSelectedAll = false
            adapter.deSelectAll()
        } else {
            isSelectedAll = true
            adapter.selectAll()
        }

        val count = adapter.selectedItemCount
        if (count == 0) {
            dismissActionMode()
        } else {
            actionMode!!.title = count.toString()
            actionMode!!.invalidate()
        }
    }

    fun unregisterReceiver() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(storagePermissionGrantedReceiver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ArtistAdapter(context, R.layout.item_track, artists)
        registerReceiver()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_track, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listView.adapter = adapter

        listView.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->
            if (actionMode != null) {
                toggleSelection(position)
                return@OnItemClickListener
            }

            val artist = parent.getItemAtPosition(position) as Artist
            val intent = Intent(activity, ArtistDetailActivity::class.java)
            intent.putExtra(IntentUtil.EXTRA_ARTIST, artist)
            startActivity(intent)
        }

        listView!!.onItemLongClickListener = AdapterView.OnItemLongClickListener { parent, v, position, id ->
            toggleSelection(position)
            adapter.setSelectMulti(true)
            true
        }

        swipe.setOnRefreshListener { refresh() }
    }

    private fun refresh() {
        loaderManager.restartLoader(ARTIST_LOADER_ID, null, this)
    }

    private fun playArtistTrack(artists: List<Artist>) {
        val tracks = ArrayList<Track>()

        for (artist in artists) {
            tracks.addAll(Util.getTracks(context,
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    MediaStore.Audio.Media.ARTIST_ID + " = " + artist.id,
                    null,
                    MediaStore.Audio.Media.TITLE + " asc"))

        }

        val intent = Intent(IntentUtil.ACTION_PLAY_ARTIST_TRACKS)
        intent.putExtra(IntentUtil.EXTRA_TRACKS, tracks as Serializable)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    private fun enableActionMode() {
        if (actionMode == null)
            actionMode = (activity as AppCompatActivity).startSupportActionMode(actionModeCallback)
    }

    private fun dismissActionMode() {
        if (actionMode != null)
            actionMode!!.finish()
    }

    private fun toggleSelection(position: Int) {
        enableActionMode()
        adapter.toggleSelection(position)

        val count = adapter.selectedItemCount
        if (count == 0) {
            dismissActionMode()
        } else {
            actionMode!!.title = count.toString()
            actionMode!!.invalidate()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (Util.checkSelfForStoragePermission(activity))
            loaderManager.initLoader(ARTIST_LOADER_ID, null, this)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver()
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val projections = arrayOf(MediaStore.Audio.Artists._ID, MediaStore.Audio.Artists.ARTIST, MediaStore.Audio.Artists.NUMBER_OF_TRACKS, MediaStore.Audio.Artists.NUMBER_OF_ALBUMS)
        return CursorLoader(context,
                MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,
                projections, null, null,
                MediaStore.Audio.Artists.ARTIST + " ASC")
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        val tmp1 = ArrayList<Artist>()
        while (data.moveToNext()) {
            val artist = Artist()
            artist.id = data.getLong(data.getColumnIndex(MediaStore.Audio.Artists._ID))
            artist.name = data.getString(data.getColumnIndex(MediaStore.Audio.Artists.ARTIST))
            artist.numAlbums = data.getInt(data.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS))
            artist.numSongs = data.getInt(data.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_TRACKS))

            tmp1.add(artist)
        }
        updateArtists(tmp1)

        if (swipe.isRefreshing) {
            swipe.isRefreshing = false
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {

    }


    private fun updateArtists(tmp: ArrayList<Artist>) {
        artists.clear()
        artists.addAll(tmp)
        adapter.notifyDataSetChanged()
    }

    companion object {
        @JvmStatic
        fun newInstance() = ArtistFragment().also {
            val args = Bundle()
            it.arguments = args
        }
    }
}
