package binh.com.music.ui

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import binh.com.music.R
import binh.com.music.data.Track
import com.woxthebox.draglistview.DragItemAdapter
import java.util.*

/**
 * Created by binh on 3/27/2019.
 */

class QueueAdapter(tracks: List<Track>,
                   private val layoutId: Int,
                   private val grabHandleId: Int,
                   private val dragOnLongPress: Boolean,
                   private val context: Context) : DragItemAdapter<Track, QueueAdapter.ViewHolder>() {

    private val selectMulti = false
    private val selectedItems: SparseBooleanArray = SparseBooleanArray()

    private var itemClickListener: OnItemClickListener? = null
    private var currentPosition = -1


    val selectedItemCount: Int
        get() = selectedItems.size()

    init {
        itemList = tracks
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return ViewHolder(view, grabHandleId, dragOnLongPress, itemClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (!selectMulti) {
            holder.checkBox.visibility = View.GONE
        } else {
            holder.checkBox.visibility = View.VISIBLE
        }

        holder.image.visibility = View.GONE
        holder.move.visibility = View.VISIBLE
        holder.checkBox.isChecked = selectedItems.get(position)

        val track = mItemList[position]

        holder.artist.text = track.artist
        holder.title.text = track.title

        if (position == currentPosition) {
            holder.artist.setTextColor(ContextCompat.getColor(context,R.color.colorAccent))
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.colorAccent))
        } else {
            holder.artist.setTextColor(ContextCompat.getColor(context,android.R.color.tab_indicator_text))
            holder.title.setTextColor(ContextCompat.getColor(context,android.R.color.tab_indicator_text))
        }
    }


    override fun getUniqueItemId(position: Int): Long {
        return mItemList[position].artistId
    }

    fun toggleSelection(pos: Int) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos)
        } else {
            selectedItems.put(pos, true)
        }

        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return mItemList.size
    }

    fun getSelectedItems(): List<Track> {
        val items = ArrayList<Track>(selectedItems.size())
        for (i in 0 until selectedItems.size()) {
            items.add(mItemList[selectedItems.keyAt(i)])
        }
        return items
    }

    fun selectAll() {
        for (i in 0 until itemCount)
            selectedItems.put(i, true)
        notifyDataSetChanged()
    }

    fun deSelectAll() {
        selectedItems.clear()
        notifyDataSetChanged()
    }


    fun setItemClickListener(itemClickListener: OnItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    fun setCurrentPosition(currentPosition: Int) {
        this.currentPosition = currentPosition
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View,
                     grabHandleId: Int,
                     dragOnLongPress: Boolean,
                     private var itemClickListener: OnItemClickListener?) : DragItemAdapter.ViewHolder(itemView, grabHandleId, dragOnLongPress) {
        val image: ImageView = itemView.findViewById(R.id.image)
        val title: TextView = itemView.findViewById(R.id.title)
        val artist: TextView = itemView.findViewById(R.id.description)
        val checkBox: CheckBox = itemView.findViewById(R.id.checkbox)
        val move: ImageView = itemView.findViewById(R.id.move)

        override fun onItemClicked(view: View) {
            itemClickListener?.onClick(view,adapterPosition)

        }

        override fun onItemLongClicked(view: View): Boolean {
itemClickListener?.onLongClick(view,adapterPosition)
            return true
        }
    }

    interface OnItemClickListener {
        fun onClick(v: View, position: Int)

        fun onLongClick(v: View, position: Int)
    }
}
