package binh.com.music

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log

/**
 * Created by binh on 3/13/2019.
 */

class App : Application() {

    companion object {

        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        if (BuildConfig.DEBUG) {
            Log.d(App::class.java.simpleName, "App create")
        }
    }


}
