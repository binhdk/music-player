package binh.com.music.util

/**
 * Created by binh on 2/26/2019.
 */

object IntentUtil {
    const val PERMISSION_STORAGE_GRANTED = "com.binh.permission.storage.granted"
    const val PLAYBACK_STATE_CHANGE = "com.binh.playback.change"
    const val ACTION_DELETE_TRACK = "com.binh.track.action.DELETE_ITEM"
    const val ACTION_DELETE_TRACKS = "com.binh.track.action.DELETE_ITEMS"

    // queue action
    const val ACTION_PLAY_QUEUE_ITEM = "com.binh.queue.action.PLAY_ITEM"
    const val ACTION_ADD_QUEUE_ITEM = "com.binh.queue.action.ADD_ITEM"
    const val ACTION_ADD_QUEUE_ITEMS = "com.binh.queue.action.ADD_ITEMS"
    const val ACTION_DELETE_QUEUE_ITEM = "com.binh.queue.action.DELETE_ITEM"
    const val ACTION_DELETE_QUEUE_ITEMS = "com.binh.queue.action.DELETE_ITEMS"

    const val ACTION_ADD_TO_FAVORITE = "com.binh.track.action.ADD_TO_FAVORITE"
    const val ACTION_CHANGE_QUEUE_ORDER = "com.binh.queue.action.CHANGE_ORDER"
    const val ACTION_PERSIST_QUEUE = "com.binh.queue.action.ACTION_PERSIST"
    const val ACTION_PLAY_PLAYLIST = "com.binh.playlist.ACTION_PLAY"
    const val ACTION_DELETE_PLAYLIST = "com.binh.playlist.action.ACTION_DELETE"
    const val ACTION_UPDATE_PLAYLIST = "com.binh.playlist.action.UPDATE"
    const val ACTION_PLAY_ALBUM = "com.binh.album.action.ACTION_PLAY"
    const val ACTION_PLAY_ARTIST_TRACKS = "com.binh.artist.action.PLAY_TRACK"


    //extras
    const val EXTRA_TRACK = "com.binh.track.extra.TRACK"
    const val EXTRA_TRACKS = "com.binh.track.extra.TRACKS"
    const val EXTRA_ALBUM = "com.binh.album.extra.ALBUM"
    const val EXTRA_ALBUMS = "com.binh.album.extra.ALBUMS"
    const val EXTRA_ARTISTS = "com.binh.album.extra.ARTISTS"
    const val EXTRA_PLAYLIST = "com.binh.playlist.extra.PLAYLIST"
    const val EXTRA_ARTIST = "com.binh.album.extra.ARTIST"
    const val EXTRA_PLAYLISTS = "com.binh.album.extra.PLAYLISTS"

    const val EXTRA_TRACK_ID = "com.binh.track.extra.TRACK_ID"

    const val ACTION_ADD_TRACKS_TO_FAVORITE = "com.binh.track.action.ADD_TRACKS_TO_FAVORITE"
    const val ACTION_ADD_PLAYLIST = "com.binh.playlist.action.ADD_PLAYLIST"
    const val ACTION_REMOVE_PLAYLIST_TRACK = "com.binh.playlist.action.REMOVE_TRACK"
    const val EXTRA_PLAYLIST_ID = "com.binh.playlist.extra.PLAYLIST_ID"
    const val ACTION_TIMER_FINISH = "com.binh.playback.timer.ACTION_FINISH"
    const val EXTRA_FAVORITE = "com.binh.track.EXTRA_FAVORITE"
}
