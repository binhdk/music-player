package binh.com.music.util

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.SQLException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.*
import android.preference.PreferenceManager
import android.support.annotation.IntDef
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaButtonReceiver
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import binh.com.music.BuildConfig
import binh.com.music.R
import binh.com.music.data.PlayList
import binh.com.music.data.Track
import binh.com.music.data.db.AppDatabase
import binh.com.music.ui.MainActivity
import binh.com.music.ui.MediaPlayerActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import java.io.Serializable
import java.util.*


/**
 * Created by binh on 3/3/2019.
 * play music in background
 */

class MediaPlayerService : Service() {
    private val binder = LocalBinder()
    private lateinit var playback: Playback
    private lateinit var mediaSession: MediaSessionCompat
    private var sessionExtras: Bundle? = null
    var metadata: MediaMetadataCompat? = null
        private set
    var playbackStateCompat: PlaybackStateCompat? = null
        private set
    private var sessionToken: MediaSessionCompat.Token? = null
    private var playIntent: PendingIntent? = null
    private var pauseIntent: PendingIntent? = null
    private var previousIntent: PendingIntent? = null
    private var nextIntent: PendingIntent? = null

    private var currentMediaId: Long = 0
    private var currentTrack: Track? = null
    var timerUntilFinished: Long = 0
        private set

    private lateinit var storagePermissionGrantedReceiver: BroadcastReceiver
    private lateinit var queueChangeReceiver: BroadcastReceiver
    private lateinit var mediaChangeReceiver: BroadcastReceiver
    private var timer: CountDownTimer? = null

    private lateinit var mediaPlayerReceiver: BroadcastReceiver

    private val mCb = object : MediaControllerCompat.Callback() {
        override fun onPlaybackStateChanged(state: PlaybackStateCompat) {
            playbackStateCompat = state
            if (state.state == PlaybackStateCompat.STATE_NONE) {
                stopForeground(false)
                stopNotification()
            } else {
                val notification = createNotification()
                if (notification != null) {
                    if (state.state == PlaybackStateCompat.STATE_PLAYING) {
                        startForeground(NOTIFICATION_ID, notification)
                    } else {
                        NotificationManagerCompat.from(this@MediaPlayerService).notify(NOTIFICATION_ID, notification)
                        stopForeground(false)
                    }
                }
            }

            val intent = Intent()
            intent.action = IntentUtil.PLAYBACK_STATE_CHANGE
            intent.putExtra("playbackstate", playbackStateCompat)
            intent.putExtra("metadata", metadata)
            LocalBroadcastManager.getInstance(this@MediaPlayerService).sendBroadcast(intent)
        }

        override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
            this@MediaPlayerService.metadata = metadata
            val notification = createNotification()
            if (notification != null) {
                if (playbackStateCompat!!.state == PlaybackStateCompat.STATE_PLAYING) {
                    startForeground(NOTIFICATION_ID, notification)
                } else {
                    NotificationManagerCompat.from(this@MediaPlayerService).notify(NOTIFICATION_ID, notification)
                    stopForeground(false)
                }
            }


            val intent = Intent()
            intent.action = IntentUtil.PLAYBACK_STATE_CHANGE
            intent.putExtra("playbackstate", playbackStateCompat)
            intent.putExtra("metadata", metadata)
            LocalBroadcastManager.getInstance(this@MediaPlayerService).sendBroadcast(intent)
        }

        override fun onSessionDestroyed() {
            super.onSessionDestroyed()
            updateSessionToken()
        }
    }

    private val playbackCallback = object : Playback.Callback {
        override fun onCompletion() {
            setPlaybackState(PlaybackStateCompat.STATE_STOPPED)
            Handler().postDelayed({ onNext() }, 2000)
        }

        override fun onPlaybackStatusChanged(state: Int) {
            setPlaybackState(state)
        }

        override fun onError(error: String) {

        }
    }
    private var albumChangeReceiver: BroadcastReceiver? = null
    private var artistChangeReceiver: BroadcastReceiver? = null
    var isEnableTimer = false
        private set

    private val origins = ArrayList<Track>()
    var currentIndex = -1
        private set
    private var playListChangeReceiver: BroadcastReceiver? = null

    private var controller: MediaControllerCompat? = null

    private val queue = ArrayList<Track>()

    val currentPosition: Int
        get() = playback.getCurrentPosition()

    val duration: Long
        get() = playback.duration

    val isPlaying: Boolean
        get() {
            if (BuildConfig.DEBUG) {
                Log.d(MediaPlayerService::class.java.simpleName, playback.isPlaying.toString() + "")
            }
            return playbackStateCompat!!.state == PlaybackStateCompat.STATE_PLAYING && playback.isPlaying
        }

    val isPrepared: Boolean
        get() = playback.isPrepared

    private val mediaSessionCallback = object : MediaSessionCompat.Callback() {
        override fun onPlay() {
            mediaSession.isActive = true
        }

        override fun onSkipToQueueItem(queueId: Long) {

        }

        override fun onSeekTo(position: Long) {

        }

        override fun onPause() {
            mediaSession.isActive = true
        }

        override fun onStop() {
            mediaSession.isActive = false
        }

        override fun onSkipToNext() {

        }

        override fun onSkipToPrevious() {

        }
    }

    var isShuffle: Boolean
        get() = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(Playback.EXTRA_SHUFFLE, false)
        set(shuffle) = PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putBoolean(Playback.EXTRA_SHUFFLE, shuffle)
                .apply()

    /**
     * @param type How playback loop queue. Either [NO_LOOP] or [LOOP_ONE]
     * [LOOP_ALL]
     */
    var loop: Int
        get() = PreferenceManager.getDefaultSharedPreferences(this)
                .getInt(Playback.EXTRA_LOOP, NO_LOOP)
        set(@Loop type) = PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putInt(Playback.EXTRA_LOOP, type)
                .apply()

    private fun onNext() {
        if (queue.isEmpty())
            return

        val loop = loop
        if (loop == LOOP_ONE) {
            play()
            return
        } else if (loop == LOOP_ALL) {
            setPlaybackState(PlaybackStateCompat.STATE_PLAYING)
        } else if (currentIndex == queue.size - 1) {
            setPlaybackState(PlaybackStateCompat.STATE_STOPPED)
        } else {
            setPlaybackState(PlaybackStateCompat.STATE_PLAYING)
        }
        next()
    }


    private fun stopNotification() {
        NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID)
    }

    private fun updateSessionToken() {

    }


    override fun onBind(intent: Intent): IBinder? {
        return binder
    }


    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Log.d(MediaPlayerService::class.java.simpleName, "onCreate")
        }
        registerReceivers()

        playback = Playback(this, playbackCallback)
        playback.state = PlaybackStateCompat.STATE_NONE

        mediaSession = MediaSessionCompat(this, MediaPlayerService::class.java.simpleName)

        sessionExtras = Bundle()
        val intent = Intent(this, MediaPlayerActivity::class.java)
        val pi = PendingIntent.getActivity(this, 99 /*request code*/,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)

        mediaSession.run {
            setCallback(mediaSessionCallback)
            setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)
            setSessionActivity(pi)
            setExtras(sessionExtras)
        }

        setPlaybackState(playback.state)

        controller = mediaSession.controller
        controller!!.registerCallback(mCb)
        sessionToken = mediaSession.sessionToken

        val tempPlayIntent = Intent(this, MediaPlayerService::class.java)
        tempPlayIntent.action = Playback.ACTION_PLAY
        playIntent = PendingIntent.getService(this, 1, tempPlayIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val tempPauseIntent = Intent(this, MediaPlayerService::class.java)
        tempPauseIntent.action = Playback.ACTION_PAUSE
        pauseIntent = PendingIntent.getService(this, 1, tempPauseIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val tempNextIntent = Intent(this, MediaPlayerService::class.java)
        tempNextIntent.action = Playback.ACTION_NEXT
        nextIntent = PendingIntent.getService(this, 1, tempNextIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val tempPrevIntent = Intent(this, MediaPlayerService::class.java)
        tempPrevIntent.action = Playback.ACTION_PREVIOUS
        previousIntent = PendingIntent.getService(this, 1, tempPrevIntent, PendingIntent.FLAG_UPDATE_CURRENT)


        updateMediaDB()
        //load current tracks
        getCurrentTracks()

        val notification = createNotification()
        if (notification != null) {
            startForeground(NOTIFICATION_ID, notification)
        }


    }

    private fun updateMediaDB() {
        origins.clear()
        origins.addAll(Util.getOriginTracks(this))
        Thread(Runnable {
            try {
                val db = AppDatabase.getInstance(applicationContext).trackDao().loadAll()
                for (track in db) {
                    if (!Util.hasIndex(origins, getTrackIndex(origins, track.id))) {
                        AppDatabase.getInstance(applicationContext).trackDao().delete(track.id)
                        AppDatabase.getInstance(applicationContext).currentTrackDao().delete(track.id)
                    }
                }
                AppDatabase.getInstance(applicationContext).trackDao().insert(origins)
            } catch (e: SQLException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }).start()
    }

    private fun setPlaybackState(state: Int) {
        playbackStateCompat = PlaybackStateCompat.Builder()
                .setActions(PlaybackStateCompat.ACTION_PAUSE or
                        PlaybackStateCompat.ACTION_PLAY or
                        PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS or
                        PlaybackStateCompat.ACTION_SKIP_TO_NEXT)
                .setState(state, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 0f)
                .build()
        mediaSession.setPlaybackState(playbackStateCompat)
    }

    fun getTrackIndex(tracks: List<Track>, id: Long): Int {
        for (track in tracks) {
            if (track.id == id)
                return tracks.indexOf(track)
        }
        return -1
    }

    fun registerReceivers() {
        val intentFilter1 = IntentFilter().also {
            it.addAction(IntentUtil.ACTION_DELETE_QUEUE_ITEM)
            it.addAction(IntentUtil.ACTION_PLAY_QUEUE_ITEM)
            it.addAction(IntentUtil.ACTION_DELETE_QUEUE_ITEMS)
            it.addAction(IntentUtil.ACTION_ADD_QUEUE_ITEM)
            it.addAction(IntentUtil.ACTION_ADD_QUEUE_ITEMS)
        }
        queueChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action ?: return
                when (action) {
                    IntentUtil.ACTION_PLAY_QUEUE_ITEM -> {
                        val track = intent.getSerializableExtra(IntentUtil.EXTRA_TRACK) as Track
                        playQueueItem(track)
                    }
                    IntentUtil.ACTION_ADD_QUEUE_ITEM -> {
                        val track = intent.getSerializableExtra(IntentUtil.EXTRA_TRACK) as Track
                        addQueueItem(track)
                    }
                    IntentUtil.ACTION_ADD_QUEUE_ITEMS -> {
                        val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as ArrayList<Track>
                        addQueueItems(tracks)
                    }
                    IntentUtil.ACTION_DELETE_QUEUE_ITEM -> {
                        val index = intent.getIntExtra("index", -1)
                        removeQueueItem(index)
                    }
                    IntentUtil.ACTION_DELETE_QUEUE_ITEMS -> {
                        val indexes = intent.getIntArrayExtra("indexs")
                        removeQueueItems(indexes)
                    }
                }
            }
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(queueChangeReceiver, intentFilter1)

        val filter = IntentFilter().also {
            it.addAction(Playback.ACTION_PAUSE)
            it.addAction(Playback.ACTION_PLAY)
            it.addAction(Playback.ACTION_NEXT)
            it.addAction(Playback.ACTION_PREVIOUS)
            it.addAction(Playback.ACTION_SHUFFLE)
            it.addAction(Playback.ACTION_LOOP_ALL)
            it.addAction(Playback.ACTION_LOOP_ONE)
            it.addAction(Playback.ACTION_NO_LOOP)
        }
        mediaPlayerReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        Playback.ACTION_NEXT -> next()
                        Playback.ACTION_PAUSE -> pause()
                        Playback.ACTION_PLAY -> play()
                        Playback.ACTION_PREVIOUS -> previous()
                        Playback.ACTION_LOOP_ALL -> loop = LOOP_ALL
                        Playback.ACTION_LOOP_ONE -> loop = LOOP_ONE
                        Playback.ACTION_NO_LOOP -> loop = NO_LOOP
                        Playback.ACTION_SHUFFLE -> {
                            val shuffle = intent.getBooleanExtra(Playback.EXTRA_SHUFFLE, false)
                            isShuffle = shuffle
                        }
                    }
                }
            }
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mediaPlayerReceiver, filter)

        storagePermissionGrantedReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                origins.clear()
                origins.addAll(Util.getOriginTracks(this@MediaPlayerService))
                getCurrentTracks()
            }
        }
        val filter5 = IntentFilter(IntentUtil.PERMISSION_STORAGE_GRANTED)

        LocalBroadcastManager.getInstance(this).registerReceiver(storagePermissionGrantedReceiver, filter5)

        mediaChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.ACTION_ADD_TO_FAVORITE -> {
                            val mediaId = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            val favorite = intent.getBooleanExtra(IntentUtil.EXTRA_FAVORITE, false)
                            if (mediaId > 0)
                                addMediaToFavorite(mediaId,favorite)
                        }
                        IntentUtil.ACTION_ADD_TRACKS_TO_FAVORITE -> {
                            val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                            val favorite = intent.getBooleanExtra(IntentUtil.EXTRA_FAVORITE, false)
                            addMediaToFavorite(tracks,favorite)
                        }
                        IntentUtil.ACTION_CHANGE_QUEUE_ORDER -> {
                            val firstPosition = intent.getIntExtra("first", -1)
                            val secondPosition = intent.getIntExtra("second", -1)
                            if (firstPosition < 0 || secondPosition < 0 || firstPosition == secondPosition)
                                return
                        }
                        IntentUtil.ACTION_DELETE_TRACK -> {
                            val mediaId = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                            if (mediaId > 0)
                                deleteQueueItemIfExist(mediaId)
                        }
                        IntentUtil.ACTION_DELETE_TRACKS -> {
                            val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>

                            deleteQueueItemsIfExist(tracks)
                        }
                    }
                }
            }
        }
        val filter6 = IntentFilter().also {
            it.addAction(IntentUtil.ACTION_ADD_TO_FAVORITE)
            it.addAction(IntentUtil.ACTION_ADD_TRACKS_TO_FAVORITE)
            it.addAction(IntentUtil.ACTION_CHANGE_QUEUE_ORDER)
            it.addAction(IntentUtil.ACTION_DELETE_TRACK)
            it.addAction(IntentUtil.ACTION_DELETE_TRACKS)
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mediaChangeReceiver, filter6)


        playListChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.ACTION_PLAY_PLAYLIST -> {
                            val playList = intent.getSerializableExtra(IntentUtil.EXTRA_PLAYLIST) as PlayList
                            addPlaylistToQueue(playList)
                        }
                    }
                }
            }
        }
        val filter7 = IntentFilter()
        filter7.addAction(IntentUtil.ACTION_PLAY_PLAYLIST)

        LocalBroadcastManager.getInstance(this).registerReceiver(playListChangeReceiver, filter7)


        albumChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.ACTION_PLAY_ALBUM -> {
                            val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                            playTracks(tracks)
                        }
                    }
                }
            }
        }
        val filter8 = IntentFilter()
        filter8.addAction(IntentUtil.ACTION_PLAY_ALBUM)

        LocalBroadcastManager.getInstance(this).registerReceiver(albumChangeReceiver, filter8)

        artistChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                if (intent != null && intent.action != null) {
                    when (intent.action) {
                        IntentUtil.ACTION_PLAY_ARTIST_TRACKS -> {
                            val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                            playTracks(tracks)
                        }
                    }
                }
            }
        }
        val filter9 = IntentFilter()
        filter9.addAction(IntentUtil.ACTION_PLAY_ARTIST_TRACKS)

        LocalBroadcastManager.getInstance(this).registerReceiver(artistChangeReceiver, filter9)

    }

    private fun addMediaToFavorite(tracks: List<Track>,favorite: Boolean) {
        for (track in tracks) {
            addMediaToFavorite(track.id,favorite)
        }
    }

    private fun playTracks(tracks: List<Track>) {
        queue.clear()
        queue.addAll(tracks)
        resetQueue()
        play()
    }

    private fun deleteQueueItemsIfExist(tracks: List<Track>) {
        val deletes = ArrayList<Track>()
        var deleteCurrent = false
        for (track in tracks) {
            if (track.id == currentMediaId)
                deleteCurrent = true
            for (q in queue) {
                if (track.id == q.id) {
                    deletes.add(q)
                }
            }
        }

        queue.removeAll(deletes)
        if (deleteCurrent) {
            resetQueue()
        } else {
            setCurrent(queue.indexOf(currentTrack))
        }

        persistQueue()

        handleQueueEmpty()

    }

    private fun handleQueueEmpty() {
        if (queue.isEmpty()) {
            playback.run {
                stop()
                reset()
            }
            mediaSession.setMetadata(null)
        }
    }


    private fun addPlaylistToQueue(playList: PlayList) {
        queue.clear()
        queue.addAll(playList.tracks)
        resetQueue()
        persistQueue()
        play()
    }

    private fun resetQueue() {
        currentMediaId = -1
        currentIndex = -1
        currentTrack = null
        this.metadata = null
        loadMetadata()
        pause()
    }

    private fun deleteQueueItemIfExist(mediaId: Long) {
        val deletes = ArrayList<Track>()
        for (track in queue) {
            if (track.id == mediaId) {
                deletes.add(track)
            }
        }

        queue.removeAll(deletes)
        if (currentMediaId == mediaId) {
            resetQueue()
        } else {
            setCurrent(queue.indexOf(currentTrack))
        }

        persistQueue()

        handleQueueEmpty()
    }


    private fun addMediaToFavorite(mediaId: Long, favorite: Boolean) {
        for (track in queue) {
            if (mediaId == track.id) {
                track.isFavorite = favorite
            }
        }
    }

    private fun playQueueItem(track: Track) {
        if (currentMediaId == track.id && isPlaying) {
            return
        }
        val index = getTrackIndex(queue, track.id)
        if (hasIndex(index)) {
            currentMediaId = track.id
            currentIndex = index
        } else {
            queue.add(track)
            currentMediaId = track.id
            currentIndex = queue.size - 1
        }

        play()
    }

    fun addQueueItems(tracks: List<Track>) {
        queue.addAll(tracks)
    }

    private fun removeQueueItems(indexes: IntArray) {
        var removeCurrent = false
        val removes = ArrayList<Track>()
        for (index in indexes) {
            if (index == currentIndex)
                removeCurrent = true
            removes.add(queue[index])
        }

        queue.removeAll(removes)
        if (removeCurrent) {
            resetQueue()
        } else if (!queue.isEmpty()) {
            setCurrent(queue.indexOf(currentTrack))
        }

        persistQueue()

        handleQueueEmpty()

    }

    fun removeQueueItem(index: Int) {
        if (hasIndex(index)) {
            if (index == currentIndex) {
                playback.stop()
                playback.reset()
                if (!hasIndex(currentIndex + 1)) {
                    currentIndex -= 1
                }
            } else if (index < currentIndex) {
                currentIndex--
            }
            queue.removeAt(index)
            if (!queue.isEmpty()) {
                setCurrent(currentIndex)
            }
        }

        persistQueue()

        handleQueueEmpty()
    }

    private fun addQueueItem(track: Track) {
        queue.add(track)
    }

    fun unregisterReceivers() {
        LocalBroadcastManager.getInstance(this).run {
            unregisterReceiver(queueChangeReceiver)
            unregisterReceiver(mediaPlayerReceiver)
            unregisterReceiver(storagePermissionGrantedReceiver)
            unregisterReceiver(mediaChangeReceiver)
            unregisterReceiver(albumChangeReceiver)
            unregisterReceiver(playListChangeReceiver)
            unregisterReceiver(artistChangeReceiver)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        playback.release()
        mediaSession.release()
        unregisterReceivers()
        stopNotification()
        persistQueue()
        if (BuildConfig.DEBUG) {
            Log.d(MediaPlayerService::class.java.simpleName, "onDestroy")
        }

        stopTimer()
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        if (!isPlaying) {
            stopSelf()
        }
    }

    private fun persistQueue() {
        for (track in queue) {
            track.position = queue.indexOf(track)
            track.isLastPlay = currentIndex == queue.indexOf(track)
        }
        val intent = Intent(applicationContext, PersistentDataService::class.java)
        intent.action = IntentUtil.ACTION_PERSIST_QUEUE
        intent.putExtra(IntentUtil.EXTRA_TRACKS, queue as Serializable)
        startService(intent)

    }

    fun isCurrentMedia(metadata: MediaMetadataCompat): Boolean {
        return playbackStateCompat!!.state == PlaybackStateCompat.STATE_PAUSED &&
                this.metadata != null &&
                this.metadata!!.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID)
                        .equals(metadata.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID), ignoreCase = true)
    }

    fun setCurrent(index: Int) {
        if (!hasIndex(index))
            return
        currentTrack = queue[index]
        currentMediaId = queue[index].id
        currentIndex = index
    }

    fun loadMetadata() {
        for (track in queue) {
            if (track.isLastPlay) {
                setCurrent(queue.indexOf(track))
                track.isLastPlay = false
            }

        }

        if ((currentIndex < 0 || currentIndex >= queue.size) && !queue.isEmpty()) {
            setCurrent(0)
        }

        for (track in queue) {
            var metadataCompat = track.metadataCompat
            if (metadataCompat.description.iconBitmap == null && metadataCompat.description.iconUri != null) {
                val albumUri = metadataCompat.description.iconUri!!.toString()
                Glide.with(this).load(albumUri)
                        .into(object : CustomTarget<Drawable>() {
                            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                                metadataCompat = MediaMetadataCompat.Builder(metadataCompat)
                                        .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, (resource as BitmapDrawable).bitmap)
                                        .putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, resource.bitmap)
                                        .build()
                                if (currentMediaId == track.id) {
                                    mediaSession.setMetadata(metadataCompat)
                                }
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {

                            }
                        })
            }
            if (track.id == currentMediaId)
                mediaSession.setMetadata(metadataCompat)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        MediaButtonReceiver.handleIntent(mediaSession, intent)

        if (BuildConfig.DEBUG) {
            Log.d(MediaPlayerService::class.java.simpleName, "onStartCommand")
        }
        if (intent != null && intent.action != null) {
            when (intent.action) {
                Playback.ACTION_NEXT -> next()
                Playback.ACTION_PAUSE -> pause()
                Playback.ACTION_PLAY -> play()
                Playback.ACTION_PREVIOUS -> previous()
            }
        }
        return Service.START_NOT_STICKY
    }

    fun previous() {
        if (queue.isEmpty())
            return

        playback.reset()
        if (currentIndex < 0 || currentIndex >= queue.size) {
            setCurrent(0)
            return
        } else if (currentIndex == 0) {
            setCurrent(queue.size - 1)
        } else {
            setCurrent(currentIndex - 1)
        }
        if (playbackStateCompat!!.state == PlaybackStateCompat.STATE_PLAYING) {
            play()
        } else if (hasIndex(currentIndex)) {
            val mediaMetadataCompat = queue[currentIndex].metadataCompat
            mediaSession.setMetadata(mediaMetadataCompat)
        }

        persistQueue()
    }

    operator fun next() {
        if (queue.isEmpty())
            return

        playback.reset()
        if (currentIndex < 0 || currentIndex >= queue.size) {
            setCurrent(0)
            return
        } else if (currentIndex == queue.size - 1) {
            setCurrent(0)
        } else {
            setCurrent(currentIndex + 1)
        }
        if (playbackStateCompat!!.state == PlaybackStateCompat.STATE_PLAYING) {
            play()
        } else if (hasIndex(currentIndex)) {
            val mediaMetadataCompat = queue[currentIndex].metadataCompat
            mediaSession.setMetadata(mediaMetadataCompat)
        }
        persistQueue()

    }

    fun pause() {
        playback.pause()
        persistQueue()
    }

    fun hasIndex(index: Int): Boolean {
        return index >= 0 && index < queue.size
    }

    fun play() {
        if (queue.isEmpty()) {
            Thread(Runnable {
                try {
                    queue.addAll(AppDatabase.getInstance(this@MediaPlayerService).trackDao().loadAll())
                    loadMetadata()
                    play()
                } catch (e: SQLException) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }
            }).start()
            return
        }
        if (hasIndex(currentIndex)) {
            val mediaMetadataCompat = queue[currentIndex].metadataCompat
            if (!isCurrentMedia(mediaMetadataCompat))
                mediaSession.setMetadata(mediaMetadataCompat)
            playback.play(MediaSessionCompat.QueueItem(
                    mediaMetadataCompat.description,
                    queue[currentIndex].id))
        } else {
            setCurrent(0)
            val mediaMetadataCompat = queue[currentIndex].metadataCompat
            if (!isCurrentMedia(mediaMetadataCompat))
                mediaSession.setMetadata(mediaMetadataCompat)
            playback.play(MediaSessionCompat.QueueItem(
                    mediaMetadataCompat.description,
                    queue[currentIndex].id))
        }

        persistQueue()
    }

    fun createNotification(): Notification? {
        if (metadata == null || playbackStateCompat == null) {
            return null
        }

        val notificationBuilder = NotificationCompat.Builder(this)

        notificationBuilder.addAction(R.drawable.ic_prev,
                getString(R.string.label_previous), previousIntent)

        addPlayPauseAction(notificationBuilder)

        notificationBuilder.addAction(R.drawable.ic_next,
                getString(R.string.label_next), nextIntent)

        val description = metadata!!.description

        var fetchArtUrl: String? = null
        var art: Bitmap? = null
        if (description.iconUri != null) {
            // This sample assumes the iconUri will be a valid URL formatted String, but
            // it can actually be any valid Android Uri formatted String.
            // async fetch the album art icon
            val artUrl = description.iconUri!!.toString()
            if (art == null) {
                fetchArtUrl = artUrl
                // use a placeholder art while the remote art is being downloaded
                art = BitmapFactory.decodeResource(resources, R.drawable.ic_track)
            }
        }

        notificationBuilder
                .setStyle(android.support.v4.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(*intArrayOf(0, 1, 2))
                        .setMediaSession(sessionToken))
                .setSmallIcon(R.drawable.ic_notification)
                .setShowWhen(false)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(createContentIntent(description))
                .setContentTitle(description.title)
                .setOngoing(playbackStateCompat!!.state == PlaybackStateCompat.STATE_PLAYING)
                .setContentText(description.subtitle)
                .setLargeIcon(art)

        if (fetchArtUrl != null) {
            fetchBitmap(fetchArtUrl, notificationBuilder)
        }
        return notificationBuilder.build()
    }

    private fun fetchBitmap(fetchArtUrl: String, notificationBuilder: NotificationCompat.Builder) {
        Glide.with(this)
                .load(fetchArtUrl)
                .into(object : CustomTarget<Drawable>() {
                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                        notificationBuilder.setLargeIcon((resource as BitmapDrawable).bitmap)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {

                    }
                })
    }

    private fun createContentIntent(description: MediaDescriptionCompat): PendingIntent {
        val backIntent = Intent(this, MainActivity::class.java)
        backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

        val intent = Intent(this, MediaPlayerActivity::class.java)
        intent.putExtra("description", description)
        return PendingIntent.getActivities(this, 99 /*request code*/,
                arrayOf(backIntent, intent), PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun addPlayPauseAction(builder: NotificationCompat.Builder) {
        val label: String
        val icon: Int
        val intent: PendingIntent?
        if (playbackStateCompat!!.state == PlaybackStateCompat.STATE_PLAYING) {
            label = getString(R.string.label_pause)
            icon = R.drawable.ic_pause
            intent = pauseIntent
        } else {
            label = getString(R.string.label_play)
            icon = R.drawable.ic_play
            intent = playIntent
        }
        builder.addAction(NotificationCompat.Action(icon, label, intent))
    }


    fun getCurrentTracks() {
        Thread(Runnable {
            try {
                queue.clear()
                val tmp = AppDatabase.getInstance(this@MediaPlayerService)
                        .currentTrackDao()
                        .tracks()

                if (tmp.isEmpty()) {
                    queue.addAll(origins)
                } else {
                    queue.addAll(tmp)
                }

            } catch (e: SQLException) {
                queue.addAll(origins)
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }

            loadMetadata()
        }).start()
    }


    fun getQueue(): List<Track> {
        return queue
    }

    fun seekTo(position: Int) {
        playback.seekTo(position)
    }

    fun stopTimer() {
        isEnableTimer = false
        timerUntilFinished = 0
        if (timer != null) {
            timer!!.cancel()
        }
    }

    fun startTimer(time: Long) {
        stopTimer()
        isEnableTimer = true
        timer = object : CountDownTimer(time, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                timerUntilFinished = millisUntilFinished
            }

            override fun onFinish() {
                timerUntilFinished = 0
                isEnableTimer = false
                playback.pause()
                val intent = Intent(IntentUtil.ACTION_TIMER_FINISH)
                LocalBroadcastManager.getInstance(this@MediaPlayerService).sendBroadcast(intent)
            }
        }.start()
    }

    fun setQueue(tracks: List<Track>, currentIndex: Int) {
        queue.clear()
        queue.addAll(tracks)
        setCurrent(currentIndex)
    }

    inner class LocalBinder : Binder() {
        // Return this instance of SignalRService so clients can call public methods
        val service: MediaPlayerService
            get() = this@MediaPlayerService
    }


    @Retention(AnnotationRetention.SOURCE)
    @IntDef(LOOP_ALL.toLong(), LOOP_ONE.toLong(), NO_LOOP.toLong())
    annotation class Loop

    companion object {
        const val NOTIFICATION_ID = 1

        const val NO_LOOP = 0
        const val LOOP_ONE = 1
        const val LOOP_ALL = 2
    }


}
