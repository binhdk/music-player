package binh.com.music.util

import android.Manifest
import android.net.Uri
import binh.com.music.BuildConfig

/**
 * Created by binh on 2/25/2019.
 */

object Constant {
    @JvmField
    val albumartUri = Uri.parse("content://media/external/audio/albumart")
    const val REQUEST_STORAGE = 0
    @JvmField
    val PERMISSIONS_STORAGE = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
    const val DRAWABLE_RES_URI = "android.resource://" + BuildConfig.APPLICATION_ID + "/drawable/"
}
