package binh.com.music.util

import android.app.IntentService
import android.content.Intent
import android.util.Log
import binh.com.music.BuildConfig
import binh.com.music.data.Track
import binh.com.music.data.db.AppDatabase
import java.util.*

/**
 * Created by binh on 3/18/2019.
 * save data when app kill
 */

class PersistentDataService : IntentService(PersistentDataService::class.java.simpleName) {
    init {
        setIntentRedelivery(true)
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null && intent.action != null) {
            when (intent.action) {
                IntentUtil.ACTION_PERSIST_QUEUE -> {
                    val queue = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                    persistQueue(queue)
                }
                IntentUtil.ACTION_DELETE_TRACK -> {
                    val mediaId = intent.getLongExtra(IntentUtil.EXTRA_TRACK_ID, -1)
                    if (mediaId > 0) {
                        deleteTracks(mediaId)
                    }
                }

                IntentUtil.ACTION_DELETE_TRACKS -> {
                    val tracks = intent.getSerializableExtra(IntentUtil.EXTRA_TRACKS) as List<Track>
                    deleteTracks(tracks)
                }
            }
        }

    }

    private fun deleteTracks(tracks: List<Track>) {
        tracks.forEach { deleteTracks(it.id) }
    }


    private fun deleteTracks(mediaId: Long) {
        try {
            AppDatabase.getInstance(this).trackDao().delete(mediaId)
            AppDatabase.getInstance(this).currentTrackDao().delete(mediaId)
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (BuildConfig.DEBUG) {
            Log.d(PersistentDataService::class.java.simpleName, "onDestroy")
        }
    }

    private fun persistQueue(queue: List<Track>) {
        try {
            val currentTracks = ArrayList<Track.CurrentTrack>()
            for (track in queue) {
                currentTracks.add(Track.CurrentTrack(track))
            }

            AppDatabase.getInstance(this).currentTrackDao().deleteAll()
            AppDatabase.getInstance(this).currentTrackDao().insert(currentTracks)
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }

    }
}
