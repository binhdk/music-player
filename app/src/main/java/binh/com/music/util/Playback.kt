package binh.com.music.util

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.PowerManager
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import binh.com.music.BuildConfig

/**
 * Created by binh on 3/3/2019.
 * Playback manage MediaPlayer object
 */

class Playback(private val context: Context, private val callback: Callback) : AudioManager.OnAudioFocusChangeListener {
    private var mediaPlayer: MediaPlayer? = null
    /**
     * Get the current [android.media.session.PlaybackState.getState]
     */
    /**
     * Set the latest playback state as determined by the caller.
     */
    var state: Int = 0
    private var currentPosition: Int = 0
    private var currentQueueItem: MediaSessionCompat.QueueItem? = null
    private var audioFocus = 0
    private var audioManager: AudioManager? = null
    var isPrepared = false
        private set

    /**
     * @return boolean indicating whether the player is playing or is supposed to be
     * playing when we gain audio focus.
     */
    val isPlaying: Boolean
        get() = mediaPlayer!!.isPlaying

    /**
     * @return duration if currently playing an item
     */
    val duration: Long
        get() = mediaPlayer!!.duration.toLong()

    init {
        createMediaPlayerIfNeeded()
    }

    private fun createMediaPlayerIfNeeded() {
        mediaPlayer = MediaPlayer()

        // Make sure the media player will acquire a wake-lock while
        // playing. If we don't do that, the CPU might go to sleep while the
        // song is playing, causing playback to stop.
        mediaPlayer!!.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK)

        // we want the media player to notify us when it's ready preparing,
        // and when it's done playing:
        mediaPlayer!!.setOnPreparedListener {
            isPrepared = true
            when (audioFocus) {
                AudioManager.AUDIOFOCUS_LOSS -> {
                    pause()
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                    pause()
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                    if (isPlaying) {
                        mediaPlayer!!.setVolume(0.3f, 0.3f)
                        state = PlaybackStateCompat.STATE_PLAYING
                        callback.onPlaybackStatusChanged(state)
                    }
                }
                AudioManager.AUDIOFOCUS_GAIN -> {
                    if (!isPlaying)
                        start()
                    mediaPlayer!!.setVolume(1.0f, 1.0f)
                }
            }
        }
        mediaPlayer!!.setOnCompletionListener { callback.onCompletion() }
        mediaPlayer!!.setOnErrorListener { mp, what, extra -> false }
        mediaPlayer!!.setOnSeekCompleteListener { }

        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioManager!!.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
    }

    fun release() {
        mediaPlayer!!.release()
        state = PlaybackStateCompat.STATE_NONE
        callback.onPlaybackStatusChanged(state)
        audioManager!!.abandonAudioFocus(this)

    }

    /**
     * Start/setup the playback.
     * Resources/listeners would be allocated by implementations.
     */
    internal fun start() {
        mediaPlayer!!.start()
        state = PlaybackStateCompat.STATE_PLAYING
        callback.onPlaybackStatusChanged(state)
    }

    internal fun getCurrentPosition(): Int {
        return mediaPlayer!!.currentPosition
    }


    /**
     * @param item to play
     */
    internal fun play(item: MediaSessionCompat.QueueItem) {
        if (audioFocus != AudioManager.AUDIOFOCUS_GAIN && audioFocus != AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
            val status = audioManager!!.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
            if (status == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                audioFocus = AudioManager.AUDIOFOCUS_GAIN
                if (currentQueueItem != null && currentPosition > 0 && currentQueueItem!!.description.mediaId != null &&
                        currentQueueItem!!.description.mediaId!!.equals(item.description.mediaId!!, ignoreCase = true)) {
                    start()
                } else {
                    try {
                        reset()
                        mediaPlayer!!.setDataSource(item.description.mediaUri!!.toString())
                        mediaPlayer!!.prepareAsync()
                    } catch (e: Exception) {
                        if (BuildConfig.DEBUG) {
                            e.printStackTrace()
                        }
                        callback.onError("Cannot play this song")
                    }

                }

                currentQueueItem = item
                currentPosition = 0
            }

        } else {
            if (currentQueueItem != null && currentPosition > 0 && currentQueueItem!!.description.mediaId != null &&
                    currentQueueItem!!.description.mediaId!!.equals(item.description.mediaId!!, ignoreCase = true)) {
                start()
            } else {
                try {
                    reset()
                    mediaPlayer!!.setDataSource(item.description.mediaUri!!.toString())
                    mediaPlayer!!.prepareAsync()
                } catch (e: Exception) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                    callback.onError("Cannot play this song")
                }

            }

            currentQueueItem = item
            currentPosition = 0
        }

    }


    /**
     * Pause the current playing item
     */
    internal fun pause() {
        mediaPlayer!!.pause()
        currentPosition = mediaPlayer!!.currentPosition
        state = PlaybackStateCompat.STATE_PAUSED
        callback.onPlaybackStatusChanged(state)
    }

    internal fun stop() {
        state = PlaybackStateCompat.STATE_NONE
        callback.onPlaybackStatusChanged(state)
    }

    /**
     * Seek to the given position
     */
    internal fun seekTo(position: Int) {
        if (isPrepared) {
            mediaPlayer!!.seekTo(position)
        }
    }

    override fun onAudioFocusChange(focusChange: Int) {
        audioFocus = focusChange
        when (focusChange) {
            AudioManager.AUDIOFOCUS_LOSS -> {
                pause()
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                pause()
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                if (isPlaying) {
                    mediaPlayer!!.setVolume(0.3f, 0.3f)
                    state = PlaybackStateCompat.STATE_PLAYING
                    callback.onPlaybackStatusChanged(state)
                }
            }
            AudioManager.AUDIOFOCUS_GAIN -> {
                if (state == PlaybackStateCompat.STATE_PLAYING) {
                    if (!isPlaying) {
                        start()
                    }
                    mediaPlayer!!.setVolume(1.0f, 1.0f)
                }
            }
        }
        callback.onPlaybackStatusChanged(state)
    }


    fun reset() {
        isPrepared = false
        mediaPlayer!!.reset()
        currentQueueItem = null
        currentPosition = 0
    }

    interface Callback {
        /**
         * On current music completed.
         */
        fun onCompletion()

        /**
         * on Playback status changed
         * Implementations can use this callback to update
         * playback state on the media sessions.
         */
        fun onPlaybackStatusChanged(state: Int)

        /**
         * @param error to be added to the PlaybackState
         */
        fun onError(error: String)
    }

    companion object {

        const val ACTION_PLAY = "play"
        const val ACTION_PAUSE = "pause"
        const val ACTION_NEXT = "next"
        const val ACTION_PREVIOUS = "prev"
        const val ACTION_SHUFFLE = "shuffle"
        const val ACTION_LOOP_ALL = "loop.all"
        const val ACTION_LOOP_ONE = "loop.one"
        const val ACTION_NO_LOOP = "no.loop"

        const val EXTRA_PLAY = "play"
        const val EXTRA_PAUSE = "pause"
        const val EXTRA_NEXT = "next"
        const val EXTRA_PREVIOUS = "prev"
        const val EXTRA_SHUFFLE = "shuffle"
        const val EXTRA_LOOP_ALL = "loop.all"
        const val EXTRA_LOOP_ONE = "loop.one"
        const val EXTRA_NO_LOOP = "no.loop"
        const val EXTRA_LOOP = "loop"
    }


}
