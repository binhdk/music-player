package binh.com.music.util

import android.Manifest
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import binh.com.music.BuildConfig
import binh.com.music.data.Track
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.Collator
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by binh on 2/22/2019.
 */

object Util {


    fun shouldShowRequestForStoragePermission(activity: Activity): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE)
    }


    fun checkSelfForStoragePermission(context: Context): Boolean {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }


    fun requestPermissions(activity: Activity, permissions: Array<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }

    fun checkSelfForWriteSettingPermission(context: Context): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.System.canWrite(context)
    }

    fun getOriginTracks(context: Context): List<Track> {
        val origins = ArrayList<Track>()
        if (Util.checkSelfForStoragePermission(context)) {
            val projections = arrayOf(MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.IS_RINGTONE, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ARTIST_ID, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.DATA)

            val resolver = context.contentResolver
            val cursor = resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    projections,
                    null, null,
                    MediaStore.Audio.Media.TITLE + " ASC")
            if (cursor != null) {
                while (cursor.moveToNext()) {

                    val id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID))
                    val title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                    val artistId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID))
                    val album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
                    val albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))
                    val duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                    val ringtone = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.IS_RINGTONE))
                    val path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))

                    val uri = ContentUris.withAppendedId(Constant.albumartUri, albumId)
                    val image = uri.toString()

                    val track = Track(id = id, title = title, artist = artist,
                            artistId = artistId, album = album, albumId = albumId,
                            duration = duration, path = path, ringtone = ringtone, image = image)
                    origins.add(track)

                }
                cursor.close()
            }
        }

        return origins

    }

    fun getTracks(context: Context, uri: Uri, selection: String?, selectionArgs: Array<String>?, sort: String?): List<Track> {
        val origins = ArrayList<Track>()
        if (Util.checkSelfForStoragePermission(context)) {
            val projections = arrayOf(MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.IS_RINGTONE, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ARTIST_ID, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.DATA)

            val resolver = context.contentResolver
            val cursor = resolver.query(uri,
                    projections,
                    selection,
                    selectionArgs,
                    sort)
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    val id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID))
                    val title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                    val artistId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID))
                    val album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
                    val albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))
                    val duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                    val ringtone = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.IS_RINGTONE))
                    val path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))

                    val albumArt = ContentUris.withAppendedId(Constant.albumartUri, albumId)
                    val image = albumArt.toString()

                    val track = Track(id = id, title = title, artist = artist,
                            artistId = artistId, album = album, albumId = albumId,
                            duration = duration, path = path, ringtone = ringtone, image = image)
                    origins.add(track)

                }
                cursor.close()
            }
        }

        return origins
    }

    fun copyFile(from: File, to: File): Boolean {
        try {
            val inputStream = FileInputStream(from)
            val out = FileOutputStream(to)

            // Copy the bits from instream to outstream
            val buf = ByteArray(1024)
            var len: Int
            do {
                len = inputStream.read(buf)
                out.write(buf, 0, len)
            } while (len > 0)
            inputStream.close()
            out.close()
            return true
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }

        return false
    }

    fun hasIndex(origin: List<*>, index: Int): Boolean {
        return index >= 0 && index < origin.size
    }

    fun deleteTracksFromDevice(context: Context, tracks: List<Track>) {
        for (track in tracks) {
            deleteTrackFromDevice(context, track)
        }

    }

    fun deleteTrackFromDevice(context: Context, track: Track) {
        try {
            val file = File(track.path)
            file.delete()
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }

        context.contentResolver
                .delete(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        MediaStore.Audio.Media._ID + " = " + track.id, null
                )

    }

    fun formatMillisecondToMMSS(milliseconds: Long): String {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)))
    }


    fun formatMillisecondToHHMM(milliseconds: Long): String {
        return String.format("%02d:%02d",
                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds)),
                TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds)))
    }

    fun getCollator(local: String): Collator {
        val lithuanian = Locale(local)
        return Collator.getInstance(lithuanian)
    }
}
